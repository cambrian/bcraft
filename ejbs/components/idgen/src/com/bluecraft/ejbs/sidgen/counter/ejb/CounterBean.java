/*************************************************************************
 *  Copyright (c) 2003-2004, Bluecraft Software. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 *************************************************************************/
// $Id: CounterBean.java,v 1.2 2004/01/05 11:28:14 hyoon Exp $

package com.bluecraft.ejbs.sidgen.counter.ejb;

import javax.ejb.EntityContext;
import javax.ejb.RemoveException;
import javax.ejb.CreateException;


public abstract class CounterBean
    implements javax.ejb.EntityBean
{
    private EntityContext context = null;
    
    // CMP field getters/setters
    //====================================
    public abstract int getCounter();
    public abstract void setCounter(int counter);
    public abstract String getName();
    public abstract void setName(String name);

    // Business Methods
    //====================================
    public int getNextValue()
    {
        int count = getCounter();
        setCounter(count + 1);
        return count;
    }
    public void setNextValue(int value)
    {
        setCounter(value);
    }
    
    // Lifecycle Methods
    //====================================
    public String ejbCreate(String name)
        throws CreateException
    {
        return ejbCreate(name, 0);
    }
    public String ejbCreate(String name, int value)
        throws CreateException
    {
        setName(name);
        setCounter(value);
        return null;
    }
    public void ejbPostCreate(String name) throws CreateException { }
    public void setEntityContext(EntityContext c)
    {
        context = c;
    }
    public void unsetEntityContext()
    {
        context = null;
    }
    public void ejbRemove() throws RemoveException { }
    public void ejbActivate() { }
    public void ejbPassivate() { }
    public void ejbStore() { }
    public void ejbLoad() { }
}
