/*************************************************************************
 *  Copyright (c) 2003-2004, Bluecraft Software. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 *************************************************************************/
// $Id: SerialIdGeneratorLocal.java,v 1.2 2004/01/05 11:28:14 hyoon Exp $

package com.bluecraft.ejbs.sidgen.ejb;


public interface SerialIdGeneratorLocal
    extends javax.ejb.EJBLocalObject
{
    public String getSerialId(String name);
    public String getSerialId(String name, int value);
    public void setSerialId(String name, int value);
}
