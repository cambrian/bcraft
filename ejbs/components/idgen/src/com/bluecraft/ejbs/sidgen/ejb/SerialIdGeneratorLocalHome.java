/*************************************************************************
 *  Copyright (c) 2003-2004, Bluecraft Software. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 *************************************************************************/
// $Id: SerialIdGeneratorLocalHome.java,v 1.2 2004/01/05 11:28:14 hyoon Exp $

package com.bluecraft.ejbs.sidgen.ejb;

import javax.ejb.CreateException;
import javax.ejb.FinderException;


public interface SerialIdGeneratorLocalHome
    extends javax.ejb.EJBLocalHome
{
    public SerialIdGeneratorLocal create() throws CreateException;
}
