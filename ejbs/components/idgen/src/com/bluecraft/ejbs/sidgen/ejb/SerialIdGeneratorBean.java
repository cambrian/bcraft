/*************************************************************************
 *  Copyright (c) 2003-2004, Bluecraft Software. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 *************************************************************************/
// $Id: SerialIdGeneratorBean.java,v 1.2 2004/01/05 11:28:14 hyoon Exp $

package com.bluecraft.ejbs.sidgen.ejb;

import javax.ejb.SessionContext;
import javax.ejb.RemoveException;
import javax.ejb.CreateException;
import javax.ejb.FinderException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.naming.InitialContext;

import com.bluecraft.ejbs.sidgen.JNDINames;
import com.bluecraft.ejbs.sidgen.counter.ejb.CounterLocal;
import com.bluecraft.ejbs.sidgen.counter.ejb.CounterLocalHome;


public class SerialIdGeneratorBean
    implements javax.ejb.SessionBean
{
    private  InitialContext ic;
    private  CounterLocalHome clh;

    // Business Methods
    //====================================
    public String getSerialId(String name)
    {
        return getSerialId(name, 0);
    }
    public String getSerialId(String name, int value)
    {
        return String.valueOf(getCounter(name, value).getNextValue());
    }
    public void setSerialId(String name, int value)
    {
        setCounter(name, value);
    }

    // Misc Methods
    //====================================
    private CounterLocal getCounter(String name)
    {
        return getCounter(name, 0);
    }
    private CounterLocal getCounter(String name, int value)
    {
        try {
            CounterLocal counter = null;
            try {
                counter = clh.findByPrimaryKey(name);
            } catch (FinderException fe) {
                counter = clh.create(name, value);
            }
            return counter;
        } catch (CreateException ce) {
            throw new EJBException("Could not create counter " + name + ". Error: " + ce.getMessage());
        }
    }
    private void setCounter(String name, int value)
    {
        try {
            CounterLocal counter = null;
            try {
                counter = clh.findByPrimaryKey(name);
                counter.setNextValue(value);
            } catch (FinderException fe) {
                counter = clh.create(name, value);
            }
        } catch (CreateException ce) {
            throw new EJBException("Could not create counter " + name + ". Error: " + ce.getMessage());
        }
    }

    // Lifecycle Methods
    //====================================
    public void ejbCreate()
    {
        try { 
            ic = new InitialContext();
            clh = (CounterLocalHome) ic.lookup(JNDINames.EJB_SIDGEN_COUNTER_EJBLOCALHOME);
        } catch (NamingException ne) {
            throw new EJBException("SerialIdGeneratorBean Got naming exception! " + ne.getMessage());
        }
    }
    public void setSessionContext(SessionContext c) { }
    public void ejbRemove() { }
    public void ejbActivate() { }
    public void ejbPassivate() { }
}

