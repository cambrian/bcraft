/*************************************************************************
 *  Copyright (c) 2003-2004, Bluecraft Software. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 *************************************************************************/
// $Id: CounterLocalHome.java,v 1.2 2004/01/05 11:28:14 hyoon Exp $

package com.bluecraft.ejbs.sidgen.counter.ejb;

import javax.ejb.CreateException;
import javax.ejb.FinderException;


public interface CounterLocalHome
    extends javax.ejb.EJBLocalHome
{
    public CounterLocal create(String name) throws CreateException;
    public CounterLocal create(String name, int value) throws CreateException;
    public CounterLocal findByPrimaryKey(String name) throws FinderException;
}

