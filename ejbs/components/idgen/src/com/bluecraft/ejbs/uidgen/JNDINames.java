/*************************************************************************
 *  Copyright (c) 2003-2004, Bluecraft Software. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 *************************************************************************/
// $Id: JNDINames.java,v 1.2 2004/01/05 11:28:14 hyoon Exp $

package com.bluecraft.ejbs.uidgen;


/**
 * Local (internal) directory of JNDI names for various EJB entities.
 * Note that any change in this class should also be reflected
 * in the corresponding deployment descriptors. 
 */
public interface JNDINames
{
    public static final String EJB_UIDGEN_EJBLOCALHOME = 
        "java:comp/env/ejb/local/bluecraft/uidgen/UniqueIdGenerator";
}

