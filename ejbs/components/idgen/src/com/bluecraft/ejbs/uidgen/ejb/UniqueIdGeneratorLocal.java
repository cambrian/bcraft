/*************************************************************************
 *  Copyright (c) 2003-2004, Bluecraft Software. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 *************************************************************************/
// $Id: UniqueIdGeneratorLocal.java,v 1.2 2004/01/05 11:28:14 hyoon Exp $

package com.bluecraft.ejbs.uidgen.ejb;


public interface UniqueIdGeneratorLocal
    extends javax.ejb.EJBLocalObject
{
    public String getUniqueId();    
}
