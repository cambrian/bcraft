/*************************************************************************
 *  Copyright (c) 2003-2004, Bluecraft Software. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 *************************************************************************/
// $Id: UniqueIdGeneratorBean.java,v 1.2 2004/01/05 11:28:14 hyoon Exp $

package com.bluecraft.ejbs.uidgen.ejb;

import com.bluecraft.common.Guid;

import javax.ejb.SessionContext;
import javax.ejb.RemoveException;
import javax.ejb.CreateException;
import javax.ejb.FinderException;
import javax.ejb.EJBException;
import javax.naming.NamingException;
import javax.naming.InitialContext;


public class UniqueIdGeneratorBean
    implements javax.ejb.SessionBean
{
    // Business Methods
    //====================================
    public String getUniqueId()
    {
        return (new Guid()).toString();
    }
    
    // Lifecycle Methods
    //====================================
    public void ejbCreate()
    {
        // ...
    }
    public void setSessionContext(SessionContext c) { }
    public void ejbRemove() { }
    public void ejbActivate() { }
    public void ejbPassivate() { }
}

