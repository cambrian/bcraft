/*************************************************************************
 *  Copyright (c) 2001-2003, BlueCraft Software. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 *************************************************************************/
// $Id: GuidFormatException.java,v 1.3 2003/02/09 08:13:51 hyoon Exp $ 

package com.bluecraft.common;


/**   
 * GuidFormatException will be thrown at run-time if Guid cannot be constructed
 * due to the incorrect format of given arguments.
 *   
 * @author   Hyoungsoo Yoon
 * @version  $Revision: 1.3 $
 */   
public class GuidFormatException
    extends RuntimeException
{
    /**
     * Constructs a new GuidFormatException.
     */
    public GuidFormatException()
    {
        super();
    }
    
    /**
     * Constructs a new GuidFormatException with the given message.
     *
     * @param message the detail message
     */
    public GuidFormatException(String message)
    {
        super(message);
    }

    /**
     * Constructs a new GuidFormatException with the specified detail message and cause. 
     *
     * @param message the detail message
     * @param cause the cause. null value means that the cause is nonexistent or unknown.
     */
    public GuidFormatException(String message, Throwable cause)
    {
        super(message, cause);
    }

    /**
     * Constructs a new GuidFormatException with the specified cause and a detail message of cause
     *
     * @param cause the cause. null value means that the cause is nonexistent or unknown.
     */
    public GuidFormatException(Throwable cause)
    {
        super(cause);
    }

}

