/*************************************************************************
 *  Copyright (c) 2003, Blueraft Software. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 *************************************************************************/
// $Id: CharHolder.java,v 1.1 2003/04/08 05:43:22 hyoon Exp $

package com.bluecraft.common;


/**
 * <i>Holder</i> class for the built-in char type.
 * 
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.1 $
 */
public final class CharHolder
        implements java.io.Serializable, Cloneable
{
    /**
     * @serial Internal char data.
     */
    private char mData;
    
    /**
     * Constructs an <i>empty</i> CharHolder.
     */
    public CharHolder()
    {
        this('\0');
    }

    /**
     * Constructs CharHolder with the givne char.
     * 
     * @param data Internal char
     */
    public CharHolder(char data)
    {
        mData = data;
    }

    /**
     * Returns the internal char data.
     * 
     * @return Internal char
     */
    public char getChar()
    {
        return mData;
    }

    /**
     * Sets the internal char with the given argument.
     * 
     * @param data Internal char to be set
     */
    public void setChar(char data)
    {
        mData = data;
    }

    /**
     * Clones this object.
     * 
     * @return Cloned CharHolder
     */
    public Object clone() 
    {
        CharHolder obj = null;

        try {
            obj = (CharHolder)(this.getClass().newInstance());
            obj.setChar(this.getChar());
        } catch (InstantiationException ie) {
            //System.err.println(ie);
            return null;
        } catch (IllegalAccessException iae) {
            //System.err.println(iae);
            return null;
        }

        return obj;
    }

    /**
     * Check if object is the same type and has the same char.
     * It overrdes Object.equals().
     * 
     * @param object Object to be compared
     * @return Return true if it has the same internal char.
     */
    public boolean equals(Object obj)
    {
        if (! (obj instanceof CharHolder)) {
            return false;
        }
        CharHolder ch = (CharHolder) obj;
        return (ch.getChar() == this.getChar());
    }
    
    /**
     * Returns a hash code.
     * 
     * @return int
     */
    public int hashCode()
    {
        return (int) this.getChar();
    }

}

