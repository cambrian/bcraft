/*************************************************************************
 *  Copyright (c) 2003, Blueraft Software. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 *************************************************************************/
// $Id: LongHolder.java,v 1.1 2003/04/08 05:43:22 hyoon Exp $

package com.bluecraft.common;


/**
 * <i>Holder</i> class for the built-in long type.
 * 
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.1 $
 */
public final class LongHolder
        implements java.io.Serializable, Cloneable
{
    /**
     * @serial internal long data.
     */
    private long mData;
    
    /**
     * Constructs an <i>empty</i> LongHolder.
     */
    public LongHolder()
    {
        this(0L);
    }

    /**
     * Constructs LongHolder with the givne long.
     * 
     * @param data internal long
     */
    public LongHolder(long data)
    {
        mData = data;
    }

    /**
     * Returns the internal long data.
     * 
     * @return Internal long
     */
    public long getLong()
    {
        return mData;
    }

    /**
     * Sets the internal long with the given argument.
     * 
     * @param data Internal long to be set
     */
    public void setLong(long data)
    {
        mData = data;
    }

    /**
     * Clones this object.
     * 
     * @return Cloned LongHolder
     */
    public Object clone() 
    {
        LongHolder obj = null;

        try {
            obj = (LongHolder)(this.getClass().newInstance());
            obj.setLong(this.getLong());
        } catch (InstantiationException ie) {
            //System.err.println(ie);
            return null;
        } catch (IllegalAccessException iae) {
            //System.err.println(iae);
            return null;
        }

        return obj;
    }

    /**
     * Check if object is the same type and has the same long.
     * It overrdes Object.equals().
     * 
     * @param object Object to be compared
     * @return Return true if it has the same internal long.
     */
    public boolean equals(Object obj)
    {
        if (! (obj instanceof LongHolder)) {
            return false;
        }
        LongHolder ch = (LongHolder) obj;
        return (ch.getLong() == this.getLong());
    }
    
    /**
     * Returns a hash code.
     * 
     * @return long
     */
    public int hashCode()
    {
        return (int) this.getLong();
    }

}

