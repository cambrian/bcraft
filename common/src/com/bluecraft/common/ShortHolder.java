/*************************************************************************
 *  Copyright (c) 2003, Blueraft Software. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 *************************************************************************/
// $Id: ShortHolder.java,v 1.1 2003/04/08 05:43:22 hyoon Exp $

package com.bluecraft.common;


/**
 * <i>Holder</i> class for the built-in short type.
 * 
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.1 $
 */
public final class ShortHolder
        implements java.io.Serializable, Cloneable
{
    /**
     * @serial internal short data.
     */
    private short mData;
    
    /**
     * Constructs an <i>empty</i> ShortHolder.
     */
    public ShortHolder()
    {
        this((short) 0);
    }

    /**
     * Constructs ShortHolder with the givne short.
     * 
     * @param data internal short
     */
    public ShortHolder(short data)
    {
        mData = data;
    }

    /**
     * Returns the internal short data.
     * 
     * @return Internal short
     */
    public short getShort()
    {
        return mData;
    }

    /**
     * Sets the internal short with the given argument.
     * 
     * @param data Internal short to be set
     */
    public void setShort(short data)
    {
        mData = data;
    }

    /**
     * Clones this object.
     * 
     * @return Cloned ShortHolder
     */
    public Object clone() 
    {
        ShortHolder obj = null;

        try {
            obj = (ShortHolder)(this.getClass().newInstance());
            obj.setShort(this.getShort());
        } catch (InstantiationException ie) {
            //System.err.println(ie);
            return null;
        } catch (IllegalAccessException iae) {
            //System.err.println(iae);
            return null;
        }

        return obj;
    }

    /**
     * Check if object is the same type and has the same short.
     * It overrdes Object.equals().
     * 
     * @param object Object to be compared
     * @return Return true if it has the same internal short.
     */
    public boolean equals(Object obj)
    {
        if (! (obj instanceof ShortHolder)) {
            return false;
        }
        ShortHolder ch = (ShortHolder) obj;
        return (ch.getShort() == this.getShort());
    }
    
    /**
     * Returns a hash code.
     * 
     * @return short
     */
    public int hashCode()
    {
        return (int) this.getShort();
    }

}

