/*************************************************************************
 *  Copyright (c) 2003, Blueraft Software. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 *************************************************************************/
// $Id: IntHolder.java,v 1.1 2003/04/08 05:43:22 hyoon Exp $

package com.bluecraft.common;


/**
 * <i>Holder</i> class for the built-in int type.
 * 
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.1 $
 */
public final class IntHolder
        implements java.io.Serializable, Cloneable
{
    /**
     * @serial Internal int data.
     */
    private int mData;
    
    /**
     * Constructs an <i>empty</i> IntHolder.
     */
    public IntHolder()
    {
        this(0);
    }

    /**
     * Constructs IntHolder with the givne int.
     * 
     * @param data Internal int
     */
    public IntHolder(int data)
    {
        mData = data;
    }

    /**
     * Returns the internal int data.
     * 
     * @return Internal int
     */
    public int getInt()
    {
        return mData;
    }

    /**
     * Sets the internal int with the given argument.
     * 
     * @param data Internal int to be set
     */
    public void setInt(int data)
    {
        mData = data;
    }

    /**
     * Clones this object.
     * 
     * @return Cloned IntHolder
     */
    public Object clone() 
    {
        IntHolder obj = null;

        try {
            obj = (IntHolder)(this.getClass().newInstance());
            obj.setInt(this.getInt());
        } catch (InstantiationException ie) {
            //System.err.println(ie);
            return null;
        } catch (IllegalAccessException iae) {
            //System.err.println(iae);
            return null;
        }

        return obj;
    }

    /**
     * Check if object is the same type and has the same int.
     * It overrdes Object.equals().
     * 
     * @param object Object to be compared
     * @return Return true if it has the same internal int.
     */
    public boolean equals(Object obj)
    {
        if (! (obj instanceof IntHolder)) {
            return false;
        }
        IntHolder ch = (IntHolder) obj;
        return (ch.getInt() == this.getInt());
    }
    
    /**
     * Returns a hash code.
     * 
     * @return int
     */
    public int hashCode()
    {
        return (int) this.getInt();
    }

}

