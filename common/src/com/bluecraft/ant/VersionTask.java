/*************************************************************************
 *  Copyright (c) 2001-2003, BlueCraft Software. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 *************************************************************************/
// $Id: VersionTask.java,v 1.5 2003/02/11 21:29:52 hyoon Exp $

package com.bluecraft.ant;

import com.bluecraft.version.*;

import java.io.EOFException;
import java.io.IOException;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.util.StringTokenizer;
import java.net.URL;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;


/**
 * Ant custom task for updating build numbers.
 * It reads the current version from the version file specified as an argument,
 * and writes back a new version with the build number increased by one.
 * 
 * @author   Hyoungsoo Yoon
 * @version  $Revision: 1.5 $
 * @see      com.bluecraft.version.IVersion
 */
public class VersionTask
    extends Task
{
    // Default version file name
    private String mFile = "buildnum.txt"; // temporary

    /**
     * The method executing the task.
     * 
     * @throws org.apache.tools.ant.BuildException
     */
    public void execute()
        throws BuildException
    {
        //System.out.println("Version file name = " + mFile);
        IVersion currentVersion = IVersion.INITIAL_VERSION;

        final ClassLoader loader = this.getClass().getClassLoader();
        //URL url = loader.getResource(mFile);
        URL url = (URL) java.security.AccessController.doPrivileged(
            new java.security.PrivilegedAction() {
                public Object run() {
                    return loader.getResource(mFile);
                }
            }
        );
        String path = mFile;
        if(url != null) {
            path = url.getPath();
            System.out.println("Version file path = " + path);
        } else {
            System.out.println("Version file name = " + path);
        }
        currentVersion = VersionUtil.readVersion(path);
        IVersion newVersion = VersionUtil.incrementBuild(currentVersion);
        System.out.println("New version = " + newVersion.toString());
        VersionUtil.writeVersion(newVersion, path);
    }

    /**
     * The setter for the "file" attribute.
     * 
     * @param name Name of the version file
     */
    public void setFile(String name)
    {
        mFile = name;
    }

}



