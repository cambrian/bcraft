/*************************************************************************
 *  Copyright (c) 2001-2003, BlueCraft Software. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 *************************************************************************/
// $Id: VersionStampTask.java,v 1.1 2003/02/11 21:29:52 hyoon Exp $

package com.bluecraft.ant;

import com.bluecraft.version.*;

import java.io.EOFException;
import java.io.IOException;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.net.URL;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;


/**
 * Ant custom task for initializing version numbers so that they can be referenced in the build file.
 * It reads the current version from the version file specified as an argument,
 * and saves various version numbers as system environment variables.
 * 
 * @author   Hyoungsoo Yoon
 * @version  $Revision: 1.1 $
 * @see      com.bluecraft.version.IVersion
 */
public class VersionStampTask
    extends Task
{
    // Default version file name
    private String mFile = "buildnum.txt"; // temporary
    // Default prefix
    private String mPrefix = "";  // No prefix by default

    // For convenience.
    private static final String DOT = ".";
    // "Namespace" prefix for version properties.
    private static final String VERSION = "version";
    // System property name: "major"
    private static final String MAJOR = "major";
    // System property name: "minor"
    private static final String MINOR = "minor";
    // System property name: "revision"
    private static final String REVISION = "revision";
    // System property name: "build"
    private static final String BUILD = "build";


    /**
     * The method executing the task.
     * 
     * @throws org.apache.tools.ant.BuildException
     */
    public void execute()
        throws BuildException
    {
        //System.out.println("Version file name = " + mFile);
        IVersion currentVersion = IVersion.INITIAL_VERSION;

        final ClassLoader loader = this.getClass().getClassLoader();
        //URL url = loader.getResource(mFile);
        URL url = (URL) java.security.AccessController.doPrivileged(
            new java.security.PrivilegedAction() {
                public Object run() {
                    return loader.getResource(mFile);
                }
            }
        );
        String path = mFile;
        if(url != null) {
            path = url.getPath();
            System.out.println("Version file path = " + path);
        } else {
            System.out.println("Version file name = " + path);
        }
        currentVersion = VersionUtil.readVersion(path);
        short major = currentVersion.getMajor();
        short minor = currentVersion.getMinor();
        short revision = currentVersion.getRevision();
        short build = currentVersion.getBuild();

        // Set system properties with all possible combinations of version numbers.
        project.setProperty(propName(this.MAJOR),propValue(major));
        project.setProperty(propName(this.MINOR),propValue(minor));
        project.setProperty(propName(this.REVISION),propValue(revision));
        project.setProperty(propName(this.BUILD),propValue(build));
        project.setProperty(propName(this.MAJOR,this.MINOR),propValue(major,minor));
        project.setProperty(propName(this.MAJOR,this.MINOR,this.REVISION),propValue(major,minor,revision));
        project.setProperty(propName(this.MAJOR,this.MINOR,this.REVISION,this.BUILD),propValue(major,minor,revision,build));
        
        // Prints out the complete version number for reference.
        System.out.println(propName(this.MAJOR,this.MINOR,this.REVISION,this.BUILD) + " = " + propValue(major,minor,revision,build));
        
        // Dump all version properties (for debugging/informational purposes)
        //java.util.Hashtable props = project.getProperties();
        //for (java.util.Enumeration e = props.keys() ; e.hasMoreElements() ;) {
        //    String name = (String) e.nextElement();
        //    if(name.startsWith(propPrefix())) {
        //        String value = project.getProperty(name);
        //        System.out.println(name + " = " + value);
        //    }
        //}
        
    }

    /**
     * The setter for the "file" attribute.
     * 
     * @param name Name of the version file
     */
    public void setFile(String name)
    {
        mFile = name;
    }

    /**
     * The setter for the "prefix" attribute.
     * This prefix will be prepended to all predefined environment variables.
     * It's not used if it's not set.
     * 
     * @param name Prefix for the version properties.
     */
    public void setPrefix(String prefix)
    {
        mPrefix = prefix;
    }


    private String propPrefix()
    {
        return propName(new String[] {});
    }
    private String propName(String name)
    {
        return propName(new String[] {name});
    }
    private String propName(String name1, String name2)
    {
        return propName(new String[] {name1, name2});
    }
    private String propName(String name1, String name2, String name3)
    {
        return propName(new String[] {name1, name2, name3});
    }
    private String propName(String name1, String name2, String name3, String name4)
    {
        return propName(new String[] {name1, name2, name3, name4});
    }

    private String propName(String[] names)
    {
        assert(names != null);

        StringBuffer sb = new StringBuffer();
        if(mPrefix != null && mPrefix.length() > 0) {
            sb.append(mPrefix);
            sb.append(DOT);
        }
        sb.append(this.VERSION);
        for(int i=0;i<names.length;i++) {
            sb.append(DOT);
            sb.append(names[i]);
        }

        return sb.toString();
    }

    private String propValue(short value)
    {
        return propValue(new short[] {value});
    }
    private String propValue(short value1, short value2)
    {
        return propValue(new short[] {value1, value2});
    }
    private String propValue(short value1, short value2, short value3)
    {
        return propValue(new short[] {value1, value2, value3});
    }
    private String propValue(short value1, short value2, short value3, short value4)
    {
        return propValue(new short[] {value1, value2, value3, value4});
    }

    private String propValue(short[] values)
    {
        assert(values != null);

        StringBuffer sb = new StringBuffer();
        for(int i=0;i<values.length;i++) {
            sb.append(values[i]);
            if(i != values.length-1) {
                sb.append(DOT);
            }
        }

        return sb.toString();
    }

}



