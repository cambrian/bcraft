/*****************************************************************************
 * Copyright (c) 2002-2003, BlueCraft Software. 
 * This software is the proprietary information of Bluecraft Software
 * and it is supplied subject to license terms.
 * See its home page (http://webstone.sourceforge.net/) for more details.
 *****************************************************************************/
// $Id: LoggerUtil.java,v 1.1 2003/12/25 06:45:39 hyoon Exp $

package com.bluecraft.util;

import com.bluecraft.common.*;
import java.util.ResourceBundle;
import java.util.logging.Logger;
import java.util.logging.Level;


/**   
 * Utility class for Java logging framework.
 *
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.1 $
 */
public abstract class LoggerUtil
{
    private static final String sName = "com.bluecraft.common";
    private static String sBundle = null;

    protected LoggerUtil()
    {
    }

    private static Logger getLogger()
    {
        return Logger.getLogger(sName,getResourceBundle());
    }

    protected static String getResourceBundle()
    {
        if(sBundle == null) {
            //sBundle = ...
        }
        return sBundle;
    }

    public static void print(String msg)
    {
        getLogger().log(Level.INFO, msg);
    }

    public static void print(Exception e, String msg)
    {
        print((Throwable)e, msg);
    }

    public static void print(Exception e)
    {
        print(e, null);
    }

    public static void print(Throwable t, String msg)
    {
        getLogger().log(Level.WARNING, "Received throwable with Message: "+msg, t);
    }

    public static void print(Throwable t)
    {
        print(t, null);
    }

}
