/*************************************************************************
 *  Copyright (c) 2003, Blueraft Software. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 *************************************************************************/
// $Id: ConsoleUtil.java,v 1.1 2003/04/08 05:43:22 hyoon Exp $

package com.bluecraft.util;

import com.bluecraft.common.*;

import java.util.HashMap;
import java.util.ArrayList;
import java.util.List;


/**
 * ConsoleUtil is a collection of utility functions.
 * 
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.1 $
 */
public class ConsoleUtil
{
    // TODO:
    private static final CharHolder FLAG_PREFIX_1 = new CharHolder('-');
    private static final CharHolder FLAG_PREFIX_2 = new CharHolder('/');
    private static List FLAG_PREFIXES = null;

    /**
     * Static initializer.
     * Initializes the default flag prefix list.
     */
    static
    {
        FLAG_PREFIXES = new ArrayList();
        FLAG_PREFIXES.add(FLAG_PREFIX_1);
        FLAG_PREFIXES.add(FLAG_PREFIX_2);
    }


    /**
     * ConsoleUtil provides only static methods.
     */
    private ConsoleUtil()
    {
    }


    /**
     * Parses the command line argument array and returns a HashMap.
     * Each entry is a pair of flag and its (optional) value.
     * Currently allowed flag prefix is '-' or '/'. But they cannot be mixed.
     * The first one in the command line argument will be used as default prefix.
     * 
     * @param args Command line argument array
     * @return HashMap of flags and their (optional) values
     */
    public static HashMap parseCommandLineArgs(String[] args)
    {
        CharHolder theFlagPrefix = new CharHolder();
        return parseCommandLineArgs(theFlagPrefix, args);
    }

    /**
     * Parses the command line argument array and returns a HashMap.
     * Each entry is a pair of flag and its (optional) value.
     * Currently allowed flag prefix is '-' or '/'. But they cannot be mixed.
     * The first one in the command line argument will be used as default prefix.
     * 
     * @param theFlagPrefix (out) the first flag prefix encountered in the given arg array
     * @param args Command line argument array
     * @return HashMap of flags and their (optional) values
     */
    public static HashMap parseCommandLineArgs(CharHolder theFlagPrefix, String[] args)
    {
        return parseCommandLineArgs(theFlagPrefix, args, FLAG_PREFIXES);
    }


    /// <summary>
    /// Parses the command line argument array and returns a HashMap.
    /// Each entry is a pair of flag and its (optional) value.
    /// </summary>
    /// <param name="args">Command line argument array</param>
    /// <param name="flagPrefix">Flag prefix</param>
    /// <returns>HashMap of flags and their (optional) values</returns>
    private static HashMap parseCommandLineArgs(String[] args, CharHolder flagPrefix)
    {
        List flagPrefixList = new ArrayList();
        flagPrefixList.add(flagPrefix);
        return parseCommandLineArgs(args, flagPrefixList);
    }


    /// <summary>
    /// Parses the command line argument array and returns a HashMap.
    /// Each entry is a pair of flag and its (optional) value.
    /// Only one flag prefix, the first char used from the given list, flagPrefixList,
    /// is chosen and is used as default prefix.
    /// </summary>
    /// <param name="args">Command line argument array</param>
    /// <param name="flagPrefixList">List of potential flag prefixes</param>
    /// <returns>HashMap of flags and their (optional) values</returns>
    private static HashMap parseCommandLineArgs(String[] args, List flagPrefixList)
    {
        CharHolder theFlagPrefix = new CharHolder();
        return parseCommandLineArgs(theFlagPrefix, args, flagPrefixList);
    }

    /// <summary>
    /// Parses the command line argument array and returns a HashMap.
    /// Each entry is a pair of flag and its (optional) value.
    /// Only one flag prefix, the first char used from the given list, flagPrefixList,
    /// is chosen and is used as default prefix.
    /// </summary>
    /// <param name="theFlagPrefix">the first flag prefix encountered in the given arg array</param>
    /// <param name="args">Command line argument array</param>
    /// <param name="flagPrefixList">List of potential flag prefixes</param>
    /// <returns>HashMap of flags and their (optional) values</returns>
    private static HashMap parseCommandLineArgs(CharHolder theFlagPrefix, String[] args, List flagPrefixList)
    {
        //theFlagPrefix = new CharHolder('\0');
        HashMap argMap = new HashMap();

        if(args == null || args.length == 0)
        {
            return argMap;
        }

        for(int i=0;i<args.length;)
        {
            //if(args[i].length == 0)
            //{
            //    i++;
            //    continue;
            //}
            int nextIndex;
            if(flagPrefixList.contains(new CharHolder(args[i].charAt(0))))
            {
                theFlagPrefix.setChar(args[i].charAt(0));

                // TODO: Make the key case-insensitive???
                String key = args[i].substring(1);
                nextIndex = parseValue(argMap, args, key, i+1, theFlagPrefix);
            }
            else
            {
                // This applies only to the first "empty" key.
                String key = "";
                nextIndex = parseValue(argMap, args, key, i, flagPrefixList);
            }
            assert (nextIndex > i);
            i = nextIndex;
        }

        return argMap;
    }

    /// <summary>
    /// Parses the command line argument array from the given index
    /// and finds the value for the given key.
    /// The value can be empty.
    /// A pair of key and its value is added to the given argMap.
    /// </summary>
    /// <param name="argMap">HashMap of command line key and value</param>
    /// <param name="args">Command line argument array</param>
    /// <param name="key">Key for which the value needs to be found from the command line argument array</param>
    /// <param name="index">Starting index from the which the value needs to be constructed</param>
    /// <param name="flagPrefix">Flag prefix</param>
    /// <returns>Index for which the next search (key and value) should be started</returns>
    private static int parseValue(HashMap argMap, String[] args, String key, int index, CharHolder flagPrefix)
    {
        List flagPrefixList = new ArrayList();
        flagPrefixList.add(flagPrefix);
        return parseValue(argMap, args, key, index, flagPrefixList);
    }


    /// <summary>
    /// Parses the command line argument array from the given index
    /// and finds the value for the given key.
    /// The value can be empty.
    /// A pair of key and its value is added to the given argMap.
    /// Only one flag prefix, the first char used from the given list, flagPrefixList,
    /// is chosen and is used as default prefix.
    /// </summary>
    /// <param name="argMap">HashMap of command line key and value</param>
    /// <param name="args">Command line argument array</param>
    /// <param name="key">Key for which the value needs to be found from the command line argument array</param>
    /// <param name="index">Starting index from the which the value needs to be constructed</param>
    /// <param name="flagPrefixList">List of potential flag prefixes</param>
    /// <returns>Index for which the next search (key and value) should be started</returns>
    private static int parseValue(HashMap argMap, String[] args, String key, int index, List flagPrefixList)
    {
        CharHolder theFlagPrefix = (CharHolder) FLAG_PREFIX_1.clone();
        return parseValue(argMap, theFlagPrefix, args, key, index, flagPrefixList);
    }

    /// <summary>
    /// Parses the command line argument array from the given index
    /// and finds the value for the given key.
    /// The value can be empty.
    /// A pair of key and its value is added to the given argMap.
    /// Only one flag prefix, the first char used from the given list, flagPrefixList,
    /// is chosen and is used as default prefix.
    /// </summary>
    /// <param name="argMap">HashMap of command line key and value</param>
    /// <param name="theFlagPrefix">the first flag prefix encountered in the given arg array</param>
    /// <param name="args">Command line argument array</param>
    /// <param name="key">Key for which the value needs to be found from the command line argument array</param>
    /// <param name="index">Starting index from the which the value needs to be constructed</param>
    /// <param name="flagPrefixList">List of potential flag prefixes</param>
    /// <returns>Index for which the next search (key and value) should be started</returns>
    private static int parseValue(HashMap argMap, CharHolder theFlagPrefix, String[] args, String key, int index, List flagPrefixList)
    {
        assert(index >= 0 && index <= args.length);
        //theFlagPrefix = new CharHolder('\0');

        int count = 0;
        int s = index;
        StringBuffer sb = new StringBuffer();
        while(true)
        {
            if(s < args.length)
            {
                String word = args[s++];
                if(flagPrefixList.contains(new CharHolder(word.charAt(0))))
                {
                    theFlagPrefix.setChar(word.charAt(0));
                    break;
                }
                else
                {
                    if(count > 0)
                    {
                        sb.append(" ");
                    }
                    sb.append(word);
                    count++;
                }
            }
            else
            {
                break;
            }
        }
        String val = sb.toString();

        if(key.length() > 0)
        {
            argMap.put(key, val);
            //System.out.println("[ConsoleUtil.parseValue()] Key = " + key + "; Value = " + val);
        }
        else
        {
            //System.out.println("[ConsoleUtil.parseValue()] Value, " + val + ", is ignored because the key is empty.");
        }

        // Reset the index so that we skip the value just parsed.
        index += count;
        return index;
    }

}

