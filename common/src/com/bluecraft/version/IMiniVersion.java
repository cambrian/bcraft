/*************************************************************************
 *  Copyright (c) 2001-2003, BlueCraft Software. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 *************************************************************************/
// $Id: IMiniVersion.java,v 1.4 2003/02/09 08:13:51 hyoon Exp $

package com.bluecraft.version;


/**
 * IMiniVersion represents "version" as a sequence of two short's.
 * Typical format is something like M.m.
 * where M is a major version number and m minor.
 * Note that IMiniVersion is read-only.
 *
 * @author   Hyoungsoo Yoon
 * @version  $Revision: 1.4 $
 */
public interface IMiniVersion
    extends Comparable
{
    /**
     * Returns the major version number.
     *
     * @return major version number
     */
    short      getMajor();

    /**
     * Returns the minor version number.
     *
     * @return minor version number
     */
    short      getMinor();

}


