/*************************************************************************
 *  Copyright (c) 2001-2003, BlueCraft Software. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 *************************************************************************/
// $Id: VersionConstants.java,v 1.6 2003/02/10 04:20:15 hyoon Exp $

package com.bluecraft.version;


/**
 * Collection constants used for Version-related classes and routines.
 * 
 * @author   Hyoungsoo Yoon
 * @version  $Revision: 1.6 $
 */
public interface VersionConstants
{
    /**
     * Version segment (major, minor, revision, build numbers) separator.
     */
    static final String VERSION_PART_DELIMITER = ".";

    /**
     * Null version.
     */
    static final IVersion NULL_VERSION = new BaseVersion((short) 0, (short) 0);

    /**
     * Version 1.0.0.1 to be used as default values.
     */
    static final IVersion DEFAULT_VERSION = new BaseVersion((short) 1, (short) 0, (short) 0, (short) 1);

    /**
     * Version 0.1.0.1 to be used as an initial value.
     */
    static final IVersion INITIAL_VERSION = new BaseVersion((short) 0, (short) 1, (short) 0, (short) 0);

}

