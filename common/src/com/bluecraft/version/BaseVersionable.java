/*************************************************************************
 *  Copyright (c) 2001-2003, BlueCraft Software. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 *************************************************************************/
// $Id: BaseVersionable.java,v 1.4 2003/02/09 08:13:51 hyoon Exp $

package com.bluecraft.version;

import java.io.EOFException;
import java.io.IOException;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.FileNotFoundException;

import java.util.StringTokenizer;


/**
 * Default implementation of the interface IVersionable.
 * 
 * @author   Hyoungsoo Yoon
 * @version  $Revision: 1.4 $
 */
public class BaseVersionable
    implements IVersionable, VersionConstants
{
    // Version of this IVersionable object.
    private IVersion   mVersion;

    /**
     * Constructs BaseVersionable with the default IVersion.
     */
    public BaseVersionable()
    {
        this(INITIAL_VERSION);
    }

    /**
     * Constructs BaseVersionable with the given IVersion.
     * 
     * @param version This BaseVersionable's version.
     */
    public BaseVersionable(IVersion version)
    {
        mVersion = version;
    }

    /**
     * Constructs BaseVersionable with the IVersion read from the given file.
     * 
     * @param fileName A file name from which IVersion will be read
     */
    public BaseVersionable(String fileName)
    {
        mVersion = VersionUtil.readVersion(fileName);
    }

    /**
     * Sets the IVersion of this object to the given argument.
     * 
     * @param version
     */
    public void       setVersion(IVersion version)
    {
        mVersion = version;
    }

    /** 
     * Returns the IVersion object associated with this object. 
     *  
     * @return IVersion
     */
    public IVersion   getVersion()
    {
        return mVersion;
    }

    /**
     * Returns true if this object's version is newer than the given version.
     * 
     * @param ver
     * @return true if this object's version is newer than the given version
     */
    public boolean    isNewerThan(IVersion ver)
    {
        int b = VersionUtil.compareVersions(mVersion, ver);
        if(b > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Returns true if this object's version is older than the given version.
     * 
     * @param ver
     * @return true if this object's version is older than the given version.
     */
    public boolean    isOlderThan(IVersion ver)
    {
        int b = VersionUtil.compareVersions(mVersion, ver);
        if(b < 0) {
            return true;
        } else {
            return false;
        }
    }

}



