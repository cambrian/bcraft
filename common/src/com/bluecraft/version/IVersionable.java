/*************************************************************************
 *  Copyright (c) 2001-2003, BlueCraft Software. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 *************************************************************************/
// $Id: IVersionable.java,v 1.4 2003/02/09 08:13:51 hyoon Exp $

package com.bluecraft.version;


/**
 * Indicates that the object is versionable.
 * A versionable object has an associated IVersion.
 * 
 * @author   Hyoungsoo Yoon
 * @version  $Revision: 1.4 $
 * @see      com.bluecraft.version.IVersion
 */
public interface IVersionable
{
    /** 
     * Returns the IVersion object associated with this object. 
     * This is a "read-only" attribute of IVersionable.
     *  
     * @return IVersion
     */
    IVersion   getVersion();

    //boolean    isNewerThan(IVersion ver);
    //boolean    isOlderThan(IVersion ver);

}

