/*************************************************************************
 *  Copyright (c) 2001-2003, BlueCraft Software. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 *************************************************************************/
// $Id: IVersion.java,v 1.7 2003/02/09 08:13:51 hyoon Exp $

package com.bluecraft.version;


/**
 * IVersion represents "a product version" as a sequence of four short's.
 * Typical format is something like M.m.r.b
 * where M is a major version number, m minor, r revision, and b build number.
 * Note that IVersion is read-only.
 *
 * @author   Hyoungsoo Yoon
 * @version  $Revision: 1.7 $
 */
public interface IVersion
    extends IMiniVersion, VersionConstants, Comparable
{    
    /**
     * Returns the revision version number.
     *
     * @return revision version number
     */
    short      getRevision();

    /**
     * Returns the build number.
     *
     * @return build number
     */
    short      getBuild();
    
//    /**
//     * Increment the build number by one and return the new IVersion object.
//     * 
//     * @return New IVersion object with the build number one bigger than this object. 
//     */
//    IVersion   incrementBuild();

    /**
     * Returns the string representation of this object.
     * The version is converted to String up to the given argument, cardinal.
     * That is, if cardinal==3, then the String representation will be something like M.m.r.
     * If, however, cardinal==0, then the version will be converted up to the last non-zero segment 
     * or up to the minor version number if the major number is the only non-zero segement.
     * 
     * @param cardinal The version is converted to String up to this argument.
     * @return String representation of this version object.
     */
    String     toString(int cardinal);

}

