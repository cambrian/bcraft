/*************************************************************************
 *  Copyright (c) 2001-2003, BlueCraft Software. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 *************************************************************************/
// $Id: VersionUtil.java,v 1.8 2003/02/11 21:29:52 hyoon Exp $

package com.bluecraft.version;

import java.io.EOFException;
import java.io.IOException;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;

import java.util.StringTokenizer;


/**
 * Collection of utility functions related to versioning.
 * 
 * @author   Hyoungsoo Yoon
 * @version  $Revision: 1.8 $
 */
public class VersionUtil
    implements VersionConstants
{
    // temporary
    private static final String DEFAULT_VERSION_FILE_NAME = "buildnum.txt";

    // Prevents instantiation
    private VersionUtil()
    {
    }

    /**
     * Returns true if the given argument is an instance of IVersion.
     *
     * @param version
     * @return true if the given argument is an instance of IVersion
     */
    public static boolean isExtendedVersion(IMiniVersion version)
    {
        if(version instanceof IVersion) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Compares two IMiniVersion objects.
     *
     * @param ver1
     * @param ver2
     * @return int
     */
    public static int compareVersions(IMiniVersion ver1, IMiniVersion ver2)
    {
        if(ver1 instanceof IVersion) {
            return ((IVersion) ver1).compareTo(ver2);
        } else {
            return ver1.compareTo(ver2);
        }
    }


    /**
     * Constructs a new IVersion object using the given argument as a template.
     * The returned object is an instance of BaseVersion.
     * Note that strVersion may not necessarily be the same as
     * new BaseVersion(strVersion).toString().
     *
     * @param strVersion the pattern string to be used to construct an IVersion object.
     * @exception com.bluecraft.version.VersionFormatException
     */
    public static IVersion parseVersion(String strVersion)
        throws VersionFormatException
    {
        //assert (strVersion != null);
        IVersion version = null;

        StringTokenizer st = new StringTokenizer(strVersion, VERSION_PART_DELIMITER);
        int numToks = st.countTokens();
        if((numToks < 2) || (numToks > 4)) {
            throw new VersionFormatException("Version String=" + strVersion + "; numTokens=" + numToks);
        }

        short major = (short) 0;
        short minor = (short) 0;
        short revision = (short) 0;
        short build = (short) 0;

        String Ma = st.nextToken();
        try {
            major = Short.parseShort(Ma);
        } catch(NumberFormatException nfx) {
            throw new VersionFormatException("Version String=" + strVersion + "; major=" + major, nfx);
        }
        String mi = st.nextToken();
        try {
            minor = Short.parseShort(mi);
        } catch(NumberFormatException nfx) {
            throw new VersionFormatException("Version String=" + strVersion + "; minor=" + minor, nfx);
        }

        if(numToks > 2) {
            String re = st.nextToken();
            try {
                revision = Short.parseShort(re);
            } catch(NumberFormatException nfx) {
                throw new VersionFormatException("Version String=" + strVersion + "; revision=" + revision, nfx);
            }
            if(numToks > 3) {
                String bi = st.nextToken();
                try {
                    build = Short.parseShort(bi);
                } catch(NumberFormatException nfx) {
                    throw new VersionFormatException("Version String=" + strVersion + "; build=" + build, nfx);
                }
            }
        }
        version = new BaseVersion(major, minor, revision, build);
        //System.out.println("Version = " + version.toString(4));

        return version;
    }

    /**
     * readVersion() reads a string or three or four strings from the default version file
     * and returns an IVersion object.
     * The version string can be given in one line (e.g. M.m.r.b),
     * or given in three or four lines, each part on a separate line.
     *
     * @return IVersion object read from the default file.
     */
    public static IVersion readVersion()
    {
        return readVersion(DEFAULT_VERSION_FILE_NAME);
    }

    /**
     * readVersion() reads a string or three or four strings from the given file
     * and returns an IVersion object.
     * The version string can be given in one line (e.g. M.m.r.b),
     * or given in three or four lines, each part on a separate line.
     *
     * @param fileName
     * @return IVersion object compatible with the string read from the given file.
     */
    public static IVersion readVersion(String fileName)
    {
        IVersion defaultVersion = INITIAL_VERSION;
        if(fileName == null || fileName.length() == 0) {
            fileName = DEFAULT_VERSION_FILE_NAME;
        }

        // read the version from VERSION_FILE_NAME
        try {
            BufferedReader in =
                new BufferedReader(
                    new FileReader(fileName));

            // Read the file
            String strLine = null;
            if((strLine = in.readLine()) != null) {
                try {
                    defaultVersion = VersionUtil.parseVersion(strLine);
                } catch(VersionFormatException vfx) {
                    StringBuffer sb = new StringBuffer(strLine);
                    while((strLine = in.readLine()) != null) {
                        sb.append(strLine);
                    }
                    defaultVersion = VersionUtil.parseVersion(sb.toString());
                }
            } else {
                System.err.println("File is empty: " + fileName);
                defaultVersion = INITIAL_VERSION;
            }
            // close it here.
            in.close();
        } catch(FileNotFoundException exc) {
            System.err.println("File Not Found: " + fileName);
            defaultVersion = INITIAL_VERSION;
        } catch(IOException iox) {
            iox.printStackTrace();
            defaultVersion = INITIAL_VERSION;
        } catch(VersionFormatException vfx) {
            defaultVersion = INITIAL_VERSION;
        }

        return defaultVersion;
    }

    /**
     * Writes the given version to the given file.
     * 
     * @param version IVersion object to be serialized
     * @param filePath
     */
    public static void writeVersion(IVersion version, String filePath)
    {
        if(filePath == null || filePath.length() == 0) {
            filePath = DEFAULT_VERSION_FILE_NAME;
        }

        // write the version to VERSION_FILE_NAME
        try {
            BufferedWriter out =
                new BufferedWriter(
                    new FileWriter(filePath));
            out.write(version.toString(4));
            out.write("\n");
            out.close();
        } catch(FileNotFoundException exc) {
            exc.printStackTrace();
        } catch(IOException iox) {
            iox.printStackTrace();
        }
    }


    /**
     * Increment the given IVersion's build number by one and return the new IVersion object.
     * 
     * @param version Reference IVersion object
     * @return New IVersion object with the build number one bigger than the given IVersion object
     */
    public static IVersion   incrementBuild(IVersion version)
    {
        return incrementBuild(version, (short) 1);
    }

    /**
     * Increment the given IVersion's build number by delta and return the new IVersion object.
     * 
     * @param version Reference IVersion object
     * @param delta  Build number delta
     * @return New IVersion object with the build number which is bigger by delta than the given IVersion object
     */
    public static IVersion   incrementBuild(IVersion version, short delta)
    {
        return new BaseVersion(version.getMajor(),version.getMinor(),version.getRevision(), (short) (version.getBuild() + delta));
    }

}


