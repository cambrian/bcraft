/*************************************************************************
 *  Copyright (c) 2001-2003, BlueCraft Software. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 *************************************************************************/
// $Id: BaseTestCase.java,v 1.3 2003/02/09 08:13:51 hyoon Exp $

package com.bluecraft.version;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import java.io.*;


/**
 * A TestCase.
 * This is just a super class for all TestCases in the version package.
 *
 * @author Hyoungsoo Yoon
 */
public abstract class BaseTestCase
    extends TestCase
{
    /**
     * Constructor.
     */
    public BaseTestCase(String s)
    {
        super(s);
    }

    /**
     * Sets up the fixture, for example, open a network connection.
     * This method is called before a test is executed.
     */
    protected void setUp()
    {
        //
    }

    /**
     * Tears down the fixture, for example, close a network connection.
     * This method is called after a test is executed
     */
    protected void tearDown()
    {
        //
    }

}

