/*************************************************************************
 *  Copyright (c) 2001-2003, BlueCraft Software. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 *************************************************************************/
// $Id: VersionUtilTestCase.java,v 1.3 2003/02/09 08:13:51 hyoon Exp $

package com.bluecraft.version;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import java.io.*;


/**
 * A TestCase.
 * This is a TestCase for VersionUtil.
 *
 * @author Hyoungsoo Yoon
 */
public class VersionUtilTestCase
    extends BaseTestCase
{
    private static final short    mMajor = (short) 1;
    private static final short    mMinor = (short) 2;
    private static final short    mBuild = (short) 3;
    private static final short    mRevision = (short) 4;
    private static final String   mVerString = "5.006";
    private static final String   mVerExString = "8.9.010.3";

    private IMiniVersion   mVersion1 = null;
    private IMiniVersion   mVersion2 = null;
    private IVersion mVersionEx1 = null;
    private IVersion mVersionEx2 = null;


    /**
     * Constructor.
     */
    public VersionUtilTestCase(String s)
    {
        super(s);
    }

    /**
     * Sets up the fixture, for example, open a network connection.
     * This method is called before a test is executed.
     */
    protected void setUp()
    {
        super.setUp();
        mVersion1 = new BaseMiniVersion(mMajor, mMinor);
        mVersion2 = new BaseMiniVersion(mVerString);
        mVersionEx1 = new BaseVersion(mMajor, mMinor, mBuild, mRevision);
        mVersionEx2 = new BaseVersion(mVerExString);
    }

    /**
     * Tears down the fixture, for example, close a network connection.
     * This method is called after a test is executed
     */
    protected void tearDown()
    {
        super.tearDown();
        mVersion1 = null;
        mVersion2 = null;
        mVersionEx1 = null;
        mVersionEx2 = null;
    }

    /**
     * Tests IsExtendedVersion().
     */
    public void testIsExtendedVersion()
    {
        boolean bExA1 = VersionUtil.isExtendedVersion(mVersion1);
        boolean bExA2 = VersionUtil.isExtendedVersion(mVersion2);
        assertTrue("BaseVersion constructed from two shorts", !bExA1);
        assertTrue("BaseVersion constructed from string", !bExA2);
        boolean bExB1 = VersionUtil.isExtendedVersion(mVersionEx1);
        boolean bExB2 = VersionUtil.isExtendedVersion(mVersionEx2);
        assertTrue("BaseVersionEx constructed from four shorts", bExB1);
        assertTrue("BaseVersionEx constructed from string", bExB2);
    }

    /**
     * Tests parseVersion().
     */
    public void testParseVersion()
        throws VersionFormatException
    {
        short    major = (short) 1;
        short    minor = (short) 2;
        short    build = (short) 3;
        short    revision = (short) 4;
        String   verString = "1.02";
        String   verExString = "1.02.003.0004";

        IMiniVersion verA = new BaseMiniVersion(major, minor);
        IMiniVersion verB = VersionUtil.parseVersion(verString);
        assertEquals("Comparing two BaseVersions contructed differently", verA, verB);
        boolean bEvA = VersionUtil.isExtendedVersion(verB);
        assertTrue("BaseVersion constructed from string with two parts", !bEvA);

        IMiniVersion verExA = new BaseVersion(major, minor, build, revision);
        IMiniVersion verExB = VersionUtil.parseVersion(verExString);
        assertEquals("Comparing two BaseVersionExs contructed differently", verExA, verExB);
        boolean bEvExA = VersionUtil.isExtendedVersion(verExB);
        assertTrue("BaseVersionEx constructed from string with four parts", bEvExA);
    }

}
