/*************************************************************************
 *  Copyright (c) 2001-2003, BlueCraft Software. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 *************************************************************************/
// $Id: VersionTestUnit.java,v 1.3 2003/02/09 08:13:51 hyoon Exp $

package com.bluecraft.version;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import java.io.*;


/**
 * A TestCase which returns TestSuite.
 * This class tests all TestCases in the version package.
 *
 * @author Hyoungsoo Yoon
 */
public class VersionTestUnit
    extends TestCase
{
    /**
     * Constructor.
     */
    public VersionTestUnit(String s)
    {
        super(s);
    }

    /**
     * Returns TestSuite.
     */
    public static Test suite()
    {
        TestSuite suite = new TestSuite();
        suite.addTestSuite(com.bluecraft.version.BaseMiniVersionTestCase.class);
        suite.addTestSuite(com.bluecraft.version.BaseVersionableTestCase.class);
        suite.addTestSuite(com.bluecraft.version.BaseVersionTestCase.class);
        suite.addTestSuite(com.bluecraft.version.VersionUtilTestCase.class);
        return suite;
    }

    /**
     * Invokes the TestRunner.
     */
    public static void main(String args[])
    {
        junit.textui.TestRunner.run(suite());
        //junit.swingui.TestRunner.run(VersionTestUnit.class);
    }

}
