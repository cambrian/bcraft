/*************************************************************************
 *  Copyright (c) 2001-2003, BlueCraft Software. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 *************************************************************************/
// $Id: BaseMiniVersionTestCase.java,v 1.2 2003/02/09 08:13:51 hyoon Exp $

package com.bluecraft.version;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import java.io.*;


/**
 * A TestCase.
 * This is a TestCase for BaseVersionable.
 *
 * @author Hyoungsoo Yoon
 */
public class BaseMiniVersionTestCase
    extends BaseTestCase
{
    private static final short    mMajor = (short) 1;
    private static final short    mMinor = (short) 2;
    private static final String   mVerString = "4.05";

    private BaseMiniVersion mVersion1 = null;
    private BaseMiniVersion mVersion2 = null;

    /**
     * Constructor.
     */
    public BaseMiniVersionTestCase(String s)
    {
        super(s);
    }

    /**
     * Sets up the fixture, for example, open a network connection.
     * This method is called before a test is executed.
     */
    protected void setUp()
    {
        super.setUp();
        mVersion1 = new BaseMiniVersion(mMajor, mMinor);
        mVersion2 = new BaseMiniVersion(mVerString);
    }

    /**
     * Tears down the fixture, for example, close a network connection.
     * This method is called after a test is executed
     */
    protected void tearDown()
    {
        super.tearDown();
        mVersion1 = null;
        mVersion2 = null;
    }

    /**
     * Tests getMajor().
     */
    public void testGetMajor()
    {
        short major = mVersion1.getMajor();
        assertEquals("Comparing major version number", mMajor, major);
    }

    /**
     * Tests getMinor().
     */
    public void testGetMinor()
    {
        short minor = mVersion1.getMinor();
        assertEquals("Comparing minor version number", mMinor, minor);
    }

    /**
     * Tests toString().
     */
    public void testToString()
    {
        String strVer2 = mVersion2.toString();
        BaseMiniVersion verA = new BaseMiniVersion(strVer2);
        String strVerA = verA.toString();
        assertEquals("Comparing string representations", strVer2, strVerA);
    }

    /**
     * Tests equals().
     */
    public void testEquals()
    {
        BaseMiniVersion verA = new BaseMiniVersion(mVerString);
        BaseMiniVersion verB = new BaseMiniVersion(mVerString);
        assertTrue("Comparing two BaseVersion constructed fromt the same string", verA.equals(verB));

        String   strA = verA.toString();
        BaseMiniVersion verC = new BaseMiniVersion(strA);
        assertTrue("Version -> String -> Version", verA.equals(verC));
    }

}

