/*************************************************************************
 *  Copyright (c) 2001-2003, BlueCraft Software. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 *************************************************************************/
// $Id: BaseVersionTestCase.java,v 1.4 2003/02/09 08:13:51 hyoon Exp $

package com.bluecraft.version;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import java.io.*;


/**
 * A TestCase.
 * This is a TestCase for BaseVersionableEx.
 *
 * @author Hyoungsoo Yoon
 */
public class BaseVersionTestCase
    extends BaseTestCase
{
    private static final short    mMajor = (short) 1;
    private static final short    mMinor = (short) 2;
    private static final short    mRevision = (short) 3;
    private static final short    mBuild = (short) 4;
    private static final String   mVerString = "5.6.7.008";

    private BaseVersion mVersion1 = null;
    private BaseVersion mVersion2 = null;

    /**
     * Constructor.
     */
    public BaseVersionTestCase(String s)
    {
        super(s);
    }

    /**
     * Sets up the fixture, for example, open a network connection.
     * This method is called before a test is executed.
     */
    protected void setUp()
    {
        super.setUp();
        mVersion1 = new BaseVersion(mMajor, mMinor, mRevision, mBuild);
        mVersion2 = new BaseVersion(mVerString);
    }

    /**
     * Tears down the fixture, for example, close a network connection.
     * This method is called after a test is executed
     */
    protected void tearDown()
    {
        super.tearDown();
        mVersion1 = null;
        mVersion2 = null;
    }

    /**
     * Tests getGuid().
     */
    public void testGetBuild()
    {
        short build = mVersion1.getBuild();
        assertEquals("Comparing build version number", mBuild, build);
    }

    /**
     * Tests getRevision().
     */
    public void testGetRevision()
    {
        short revision = mVersion1.getRevision();
        assertEquals("Comparing revision version number", mRevision, revision);
    }

    /**
     * Tests toString().
     */
    public void testToString()
    {
        String strVer2 = mVersion2.toString();
        BaseVersion verA = new BaseVersion(strVer2);
        String strVerA = verA.toString();
        assertEquals("Comparing string representations", strVer2, strVerA);
    }

    /**
     * Tests equals().
     */
    public void testEquals()
    {
        BaseVersion verA = new BaseVersion(mVerString);
        BaseVersion verB = new BaseVersion(mVerString);
        assertTrue("Comparing two BaseVersionEx constructed fromt the same string", verA.equals(verB));

        String   strA = verA.toString();
        BaseVersion verC = new BaseVersion(strA);
        assertTrue("VersionEx -> String -> VersionEx", verA.equals(verC));
    }

}
