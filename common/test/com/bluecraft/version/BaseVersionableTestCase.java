/*************************************************************************
 *  Copyright (c) 2001-2003, BlueCraft Software. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 *************************************************************************/
// $Id: BaseVersionableTestCase.java,v 1.4 2003/02/09 08:13:51 hyoon Exp $

package com.bluecraft.version;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import java.io.*;


/**
 * A TestCase.
 * This is a TestCase for BaseVersionable.
 *
 * @author Hyoungsoo Yoon
 */
public class BaseVersionableTestCase
    extends BaseTestCase
{
    private BaseVersionable mBaseVersionable = null;

    /**
     * Constructor.
     */
    public BaseVersionableTestCase(String s)
    {
        super(s);
    }

    /**
     * Sets up the fixture, for example, open a network connection.
     * This method is called before a test is executed.
     */
    protected void setUp()
    {
        super.setUp();
        mBaseVersionable = new BaseVersionable();
    }

    /**
     * Tears down the fixture, for example, close a network connection.
     * This method is called after a test is executed
     */
    protected void tearDown()
    {
        super.tearDown();
        mBaseVersionable = null;
    }

    /**
     * Tests getVersion() and setVersion().
     */
    public void testGetSetVersion()
    {
        IVersion version1 = new BaseVersion((short) 1, (short) 2);
        mBaseVersionable.setVersion(version1);
        IVersion version2 = mBaseVersionable.getVersion();
        assertEquals("Get/Set BaseVersios", version1, version2);

        IVersion version3 = new BaseVersion((short) 1, (short) 2, (short) 3, (short) 4);
        mBaseVersionable.setVersion(version3);
        IVersion version4 = mBaseVersionable.getVersion();
        assertEquals("Get/Set BaseVersioExs", version3, version4);
    }

}
