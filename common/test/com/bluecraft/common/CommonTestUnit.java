/*************************************************************************
 *  Copyright (c) 2001-2003, BlueCraft Software. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 *************************************************************************/
// $Id: CommonTestUnit.java,v 1.2 2003/02/09 08:13:51 hyoon Exp $

package com.bluecraft.common;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import java.io.*;


/**
 * A TestCase which returns TestSuite.
 * This class tests all TestCases in the common package.
 *
 * @author Hyoungsoo Yoon
 */
public class CommonTestUnit
    extends TestCase
{
    /**
     * Constructor.
     */
    public CommonTestUnit(String s)
    {
        super(s);
    }

    /**
     * Returns TestSuite.
     */
    public static Test suite()
    {
        TestSuite suite = new TestSuite();
        suite.addTestSuite(GuidTestCase.class);
        return suite;
    }

    /**
     * Invokes the TestRunner.
     */
    public static void main(String args[])
    {
        junit.textui.TestRunner.run(suite());
        //junit.swingui.TestRunner.run(CommonTestUnit.class);
    }

}

