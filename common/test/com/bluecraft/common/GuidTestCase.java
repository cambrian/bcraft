/*************************************************************************
 *  Copyright (c) 2001-2003, BlueCraft Software. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 *************************************************************************/
// $Id: GuidTestCase.java,v 1.3 2003/02/10 04:20:15 hyoon Exp $

package com.bluecraft.common;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import java.io.*;


/**
 * A TestCase for Guid.
 *
 * @author Hyoungsoo Yoon
 */
public class GuidTestCase
    extends BaseTestCase
{
    /**
     * Constructor.
     */
    public GuidTestCase(String s)
    {
        super(s);
    }

    /**
     * Sets up the fixture, for example, open a network connection.
     * This method is called before a test is executed.
     */
    protected void setUp()
    {
        super.setUp();
        //
    }

    /**
     * Tears down the fixture, for example, close a network connection.
     * This method is called after a test is executed
     */
    protected void tearDown()
    {
        super.tearDown();
        //
    }

    /**
     * Tests read().
     * @todo Not implemented yet.
     */
    public void testRead()
    { 
//          DataInput in1=  null  /** @todo fill in non-null value */;
//          try {
//              Guid guidRet = Guid.read(in1);
//              /** @todo  Insert test code here.  Use assertEquals(), for example. */
//          }
//          catch(Exception e) {
//              System.err.println("Exception thrown:  "+e);
//          }
    }

    /**
     * Tests write().
     * @todo Not implemented yet.
     */
    public void testWrite()
    { 
//          Guid guid = new Guid();
//          DataOutput out1=  null  /** @todo fill in non-null value */;
//          try {
//              guid.write(out1);
//              /** @todo  Insert test code here.  Use assertEquals(), for example. */
//          }
//          catch(Exception e) {
//              System.err.println("Exception thrown:  "+e);
//          }
    }

    
    /**
     * Tests toString().
     */
    public void testToString()
    {
        Guid guid1 = new Guid();
        String strGuid2 = guid1.toString();
        Guid guid2 = new Guid(strGuid2);
        String strGuid3 = guid2.toString();
        assertEquals("Guid -> String -> Guid -> String", strGuid2, strGuid3);
    }

    /**
     * Tests equals().
     */
    public void testEquals()
    { 
        String strGuid1 = "1-2-3-4";
        Guid guid1 = new Guid(strGuid1);
        String strGuid2 = guid1.toString();
        Guid guid2 = new Guid(strGuid2);
        assertTrue("Sting -> Guid -> String -> Guid", guid1.equals(guid2));
    }
    
    /**
     * Tests hashCode().
     */
    public void testHashCode()
    { 
        Guid guid1 = new Guid();
        int intRet1 = guid1.hashCode();
        Guid guid2 = new Guid();
        int intRet2 = guid2.hashCode();
        assertTrue("Two different Guids", intRet1 != intRet2);

        Guid guid3 = new Guid();
        int intRet3 = guid3.hashCode();
        Guid guid4 = new Guid(guid3.toString());
        int intRet4 = guid4.hashCode();
        assertTrue("Same Guids (different instances)", intRet3 == intRet4);
    }

}
