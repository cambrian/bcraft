/*************************************************************************
 *  Copyright (c) 2001-2003, BlueCraft Software. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 *************************************************************************/
// $Id: CommonTestSuite.java,v 1.2 2003/02/09 08:13:51 hyoon Exp $

package com.bluecraft;

import com.bluecraft.common.*;
import com.bluecraft.version.*;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import java.io.*;


/**
 * A TestCase which returns TestSuite.
 * This class tests all TestUnits in the common project.
 *
 * @author Hyoungsoo Yoon
 */
public class CommonTestSuite
    extends TestCase
{
    /**
     * Constructor.
     */
    public CommonTestSuite(String s)
    {
        super(s);
    }

    /**
     * Returns TestSuite.
     */
    public static Test suite()
    {
        TestSuite suite = new TestSuite();
        suite.addTest(CommonTestUnit.suite());
        suite.addTest(VersionTestUnit.suite());
        return suite;
    }

    /**
     * Invokes the TestRunner.
     */
    public static void main(String args[])
    {
        junit.textui.TestRunner.run(suite());
        //junit.swingui.TestRunner.run(CommonTestSuite.class);
    }

}

