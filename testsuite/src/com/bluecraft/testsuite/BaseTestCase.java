/************************************************************ 
 *  Copyright 2002, BlueCraft Software. All Rights Reserved.      
 *  This software is distributed on an "AS IS" basis,         
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.  
 ************************************************************/
// $Id: BaseTestCase.java,v 1.1 2003/01/05 09:27:41 hyoon Exp $

package com.bluecraft.testsuite;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import java.io.*;


/**
 * A TestCase.
 * This is just a prototype.
 *
 * @author Hyoungsoo Yoon
 */
public abstract class BaseTestCase
    extends TestCase
{
    /**
     * Constructor.
     */
    public BaseTestCase(String s)
    {
        super(s);
    }

    /**
     * Sets up the fixture, for example, open a network connection.
     * This method is called before a test is executed.
     */
    protected void setUp()
    {
        //
    }

    /**
     * Tears down the fixture, for example, close a network connection.
     * This method is called after a test is executed
     */
    protected void tearDown()
    {
        //
    }

    /**
     * A dummy test function which is supposed succeed.
     */
    public void testDummy()
    {
        Boolean b_expected = new Boolean(true);
        Boolean b_actual = new Boolean(true);
        assertEquals("Comparing two Booleans", b_expected, b_actual);
    }

}

