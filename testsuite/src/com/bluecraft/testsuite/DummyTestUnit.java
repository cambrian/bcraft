/************************************************************ 
 *  Copyright 2002, BlueCraft Software. All Rights Reserved.      
 *  This software is distributed on an "AS IS" basis,         
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.  
 ************************************************************/
// $Id: DummyTestUnit.java,v 1.1 2003/01/05 09:27:41 hyoon Exp $

package com.bluecraft.testsuite;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;


/**
 * A dummy TestCase which returns TestSuite.
 * This is just a prototype.
 *
 * @author Hyoungsoo Yoon
 */
public class DummyTestUnit
    extends TestCase
{
    /**
     * Constructor.
     */
    public DummyTestUnit(String s)
    {
        super(s);
    }

    /**
     * Returns TestSuite.
     */
    public static Test suite()
    {
        TestSuite suite = new TestSuite();
        suite.addTestSuite(DummyTestCase.class);
        return suite;
    }

    /**
     * Invokes the TestRunner.
     */
    public static void main(String args[])
    {
        junit.textui.TestRunner.run(suite());
        //junit.swingui.TestRunner.run(DummyTestUnit.class);
    }

}

