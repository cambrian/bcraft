/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. This software is distributed on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 *************************************************************************/
// $Id: GenericBean.java,v 1.2 2003/02/02 23:08:41 hyoon Exp $

package com.bluecraft.web;

import  com.bluecraft.common.*;

import java.util.*;


/**
 * GenericBean is a superclass of all "Beans" in the bluecraft.web package.
 *
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */
public abstract class GenericBean
    implements IBean, Cloneable
{
    // Each instance of GenericBean will have a unique Guid.
    // Note that once the bean is created, its Guid cannot be changed.
    private final Guid   mGuid;

    /**
     * Default Cotr
     */
    public GenericBean()
    {
        mGuid = new Guid();
    }

    /**
     * Returns the Guid of this Bean.
     */
    public Guid   getGuid()
    {
        return mGuid;
    }

    // Cloneable
    protected Object clone() 
    {
        GenericBean bean = null;

        try {
            bean = (GenericBean)(this.getClass().newInstance());
            //bean.mGuid = new Guid(); // Cloned bean will have a different Guid.
    	} catch (InstantiationException ie) {
    		System.err.println(ie);
            return this;
        } catch (IllegalAccessException iae) {
        	System.err.println(iae);
            return this;
        }

        return bean;
    }

}

