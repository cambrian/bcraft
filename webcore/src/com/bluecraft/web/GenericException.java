/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. This software is distributed on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 *************************************************************************/
// $Id: GenericException.java,v 1.2 2003/02/02 23:08:41 hyoon Exp $

package com.bluecraft.web;

import  com.bluecraft.common.*;


/**
 * GenericException is a super class of all exceptions thrown from the com.bluecraft.web package.
 *   
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */   
public class GenericException
    extends Exception
{
    /**
     * Constructs a new GenericException.
     */
    public GenericException()
    {
        super();
    }
    
    /**
     * Constructs a new GenericException with the given message.
     *
     * @param message the detail message
     */
    public GenericException(String message)
    {
        super(message);
    }

    /**
     * Constructs a new GenericException with the specified detail message and cause. 
     *
     * @param message the detail message
     * @param cause the cause. null value means that the cause is nonexistent or unknown.
     */
    public GenericException(String message, Throwable cause)
    {
        super(message, cause);
    }

    /**
     * Constructs a new GenericException with the specified cause and a detail message of cause
     *
     * @param cause the cause. null value means that the cause is nonexistent or unknown.
     */
    public GenericException(Throwable cause)
    {
        super(cause);
    }

}

