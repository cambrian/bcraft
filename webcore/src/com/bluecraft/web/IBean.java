/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. This software is distributed on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 *************************************************************************/
// $Id: IBean.java,v 1.2 2003/02/02 23:08:41 hyoon Exp $

package com.bluecraft.web;

import  com.bluecraft.common.*;


/**
 * All "Beans" in the com.bluecraft.web package implements this <code>IBean</code> interface.
 *
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */
public interface IBean
{
    /**
     * Returns the Guid of this Bean.
     */
    Guid   getGuid();
}

