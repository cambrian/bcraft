/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. This software is distributed on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 *************************************************************************/
// $Id: PaginatedListView.java,v 1.2 2003/02/02 23:08:41 hyoon Exp $

package com.bluecraft.web.ui;

import  com.bluecraft.common.*;
import  com.bluecraft.web.*;

import java.util.*;


/**
 * The <code>PaginatedListView</code> object implements convenience routines
 * which manipulate items contained in a page.
 * The items need to be a type of IBean.
 *
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */
public class PaginatedListView
    extends PaginationView
    implements IPaginatedListView
{
    //
    private List  mItems = new ArrayList();  // IBean[]
    
    
    /**
     * Default constructor.
     * Need this for cloning.
     */
    public PaginatedListView()
    {
        this(null);
    }

    /**
     * Constructs PaginatedListView with the given argument.
     * 
     * @param items
     */
    public PaginatedListView(IBean[] items)
    {
        setItems(items);
    }
    
    protected List getItemList()
    {
        return mItems;
    }


    private synchronized void  setItems(IBean[] items)
    {
        //System.out.println("PaginatedListView.setItems(). items = " + items);

        if(items != null) {
            int len = items.length;
            for(int i=0;i<len;i++) {
                mItems.add(items[i]);
            }
            setItemCount(len);
        } else {
            setItemCount(0);
        }
    }
    
    public synchronized void  addItem(IBean item)
    {
        if(mItems.add(item)) {
            incrItemCount();
        }
    }
    
    public synchronized void  removeItem(IBean item)
    {
        if(mItems.remove(item)) {
            decrItemCount();
        }
    }

    /**
     * Returns the Item/IBean for the given index.
     *
     * @param index index.
     * @return an <code>IBean</code> value
     */
    public IBean  getItem(int index)
    {
        return (IBean) mItems.get(index);
    }


    // Cloneable
    public Object clone() 
    {
        PaginatedListView bean = (PaginatedListView) super.clone();
            
        //...
        //bean.mItems = mItems;
        bean.mItems = new ArrayList();
        if(mItems != null) {
            for(int i=0;i<mItems.size();i++){
                bean.mItems.add(mItems.get(i));
            }
        }
        
        //
        return bean;
    }

}

