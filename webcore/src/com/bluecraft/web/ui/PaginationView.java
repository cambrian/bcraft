/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. This software is distributed on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 *************************************************************************/
// $Id: PaginationView.java,v 1.2 2003/02/02 23:08:41 hyoon Exp $

package com.bluecraft.web.ui;

import  com.bluecraft.common.*;
import  com.bluecraft.web.*;

import java.util.*;
import java.io.IOException;


/**
 * PaginationView is an implementation class of IPaginationView.
 * The class provides paging-related methods and fields.
 *
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */
public class PaginationView
    extends GenericBean
    implements IPaginationView
{
    private static int   INFINITE_ITEMS_IN_PAGE = Integer.MAX_VALUE;  // no pagination!
    private static int   DEFAULT_MAX_ITEMS_IN_PAGE = INFINITE_ITEMS_IN_PAGE;  // temporary
    
    private  int mMaximumNumberOfItemsInPage;
    private  int mItemCount;
    
    // mCurrentCounter can be outside the valid range (from 0 to mItemCount-1).
    private  int mCurrentCounter;
    

    /**
     * Default constructor.
     * Need this for cloning.
     */
    public PaginationView()
    {
        this(DEFAULT_MAX_ITEMS_IN_PAGE);
    }

    /**
     * Constructs PaginationView with the given argument.
     * 
     * @param maximumNumberOfItemsInPage
     */
    public PaginationView(int maximumNumberOfItemsInPage)
    {
        mMaximumNumberOfItemsInPage = maximumNumberOfItemsInPage;
        mItemCount = 0;
        mCurrentCounter = 0; // ???
    }


    // CurrentCounter runs from 0 to mItemCount-1.
    protected int           getCurrentCounter()
    {
        // returns the "kosher" value.
        if(mCurrentCounter < 0) {
            return 0;
        } else if(mCurrentCounter >= mItemCount) {
            return mItemCount -1;
        } else {
            return mCurrentCounter;
        }
    }

    protected void          setCurrentCounter(int counter)
    {
        // Should we allow only "kosher" values???
        mCurrentCounter = counter;
    }


    /**
     * Returns the default/maximum number of items/objects per page.
     *
     * @return maximum number items in a page. 
     */
    public int              getMaximumNumberOfItemsInPage()
    {
        return mMaximumNumberOfItemsInPage;
    }

    public void             setMaximumNumberOfItemsInPage(int num)
    {
        if(num > 0) {
            mMaximumNumberOfItemsInPage = num;
        } else if(num == -1) {
            mMaximumNumberOfItemsInPage = INFINITE_ITEMS_IN_PAGE;
        } else {
            //System.out.println("MaximumNumberOfItemsInPage should be a positive number or -1 (for no pagination).");
            mMaximumNumberOfItemsInPage = DEFAULT_MAX_ITEMS_IN_PAGE;
        }
    }

    /**
     * Returns the total number of items/objects in the entire "list".
     *
     * @return item count.
     */
    public int              getItemCount()
    {
        return mItemCount;
    }
    
    public void             setItemCount(int count)
    {
        if(count >= 0) {
            mItemCount = count;
        } else {
            //System.out.println("ItemCount should be positive or zero.");
            mItemCount = 0;
        }
    }

    protected void             incrItemCount()
    {
        addItemCount(1);
    }

    protected void             decrItemCount()
    {
        addItemCount(-1);
    }

    protected void             addItemCount(int delta)
    {
        int temp = mItemCount + delta;
        if(temp >= 0) {
            mItemCount = temp;
        } else {
            //System.out.println("ItemCount should be positive or zero.");
            mItemCount = 0;
        }
    }
    
    /**
     * Returns the total number of pages.
     *
     * @return page count.
     */
    public int              getPageCount()
    {
        int count = (mItemCount - 1) / mMaximumNumberOfItemsInPage + 1;
        
        // pagecount returns value >= 1 even if mItemCount == 0.
        // Check: is it goood?
        return count;
    }
    
    /**
     * Returns the page number of the "current" item.
     * Page number runs from 1 to getPageCount().
     * It returns 1 even if the list is empty, i.e. the current item is null.
     *
     * @return page number of the current item.
     */
    public int              getPageNumber()
    {
        int page = getCurrentCounter() / mMaximumNumberOfItemsInPage + 1;
        
        // Page starts from 1.
        return page;
    }
    
    /**
     * Returns the index of the first item in the page.
     * It returns 0 even if the list is empty, i.e. the current item is null.
     * (index runs from 0 to getItemCount()-1.)
     *
     * @return index of the first page.
     */
    public int              getBeginIndex()
    {
        return getBeginIndex(getPageNumber());
    }
    
    /**
     * Returns the (index+1) of the last item in the page.
     * (index runs from 0 to getItemCount()-1.)
     *
     * @return last index.
     */
    public int              getEndIndex()
    {
        return getEndIndex(getPageNumber());
    }

    // It still returns 0 even if the list is totally empty.
    private int              getBeginIndex(int page)
    {
        int  begin = (page - 1) * mMaximumNumberOfItemsInPage;
        
        // index runs from 0 to mItemCount-1.
        return begin;
    }
    
    // It returns the (index+1) of the last item in the page
    private int              getEndIndex(int page)
    {
        int endPage = getBeginIndex(page) + mMaximumNumberOfItemsInPage;
        int endAll = mItemCount;
        
        // If the page is not completely populated (for last page), we return the index of the last item.
        return (endPage < endAll) ? endPage : endAll;
    }

    /**
     * Tests if the page number is greater than 1.
     *
     * @return true if the page number is greater than 1; false otherwise.
     */
    public boolean          hasPrevious()
    {
        if(getPageNumber() > 1) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * Returns true if the page number is smaller than getPageCount().
     *
     * @return true if the page number is smaller than getPageCount(); false otherwise.
     */
    public boolean          hasNext()
    {
        if(getPageNumber() < getPageCount()) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * Returns true if the given page number greater than or equal to 1 
     * and if it is smaller than or equals to getPageCount().
     *
     * @param page page number.
     * @return True if the page is valid; false otherwise.
     */
    public boolean          isValidPage(int page)
    {
        if(page >= 1 && page <= getPageCount()) {
            return true;
        } else {
            return false;
        }
    }
    

    /**
     * Go to the previous page.
     * If the operation fails for any reason, it returns false.
     *
     * @return True if previous page is accessible; false otherwise.
     */
    public boolean          previousPage()
    {
        if(!hasPrevious()) {
            return false;
        }
        
        int firstIndex = getBeginIndex();
        int counter = firstIndex - mMaximumNumberOfItemsInPage;
        
        // Check if counter is valid... 
        mCurrentCounter = counter;
        return true;
    }
    
    /**
     * Go to the next page.
     * If the operation fails for any reason, it returns false.
     *
     * @return True if next page is accessible; false otherwise.
     */
    public boolean          nextPage()
    {
        if(!hasNext()) {
            return false;
        }
        
        int firstIndex = getBeginIndex();
        int counter = firstIndex + mMaximumNumberOfItemsInPage;
        
        // Check if counter is valid... 
        mCurrentCounter = counter;
        return true;
    }
    
    /**
     * Go to the given page.
     * If the operation fails for any reason, it returns false.
     *
     * @param page specified page number.
     * @return True if the page is accessible; false otherwise.
     */
    public boolean          gotoPage(int page)
    {
        if(!isValidPage(page)) {
            return false;
        }
        
        int firstIndex = getBeginIndex(page);
        int counter = firstIndex + mMaximumNumberOfItemsInPage;
        
        // Check if firstIndex is valid... 
        mCurrentCounter = firstIndex;
        return true;
    }

    /**
     * Go to the first page.
     * If the operation fails for any reason, it returns false.
     *
     * @return True if can goto the first page; false otherwise.
     */
    public boolean          gotoFirstPage()
    {
        // Always returns true...
        return gotoPage(1);
    }

    /**
     * Go to the last page.
     * If the operation fails for any reason, it returns false.
     *
     * @return True if can goto the last page; false otherwise.
     */
    public boolean          gotoLastPage()
    {
        // Always returns true...
        return gotoPage(getPageCount());
    }


    // FN_PAGE_GOTO_NEXT
    protected boolean isRequestGotoNext()
    {
        //System.out.println("Parsing " + FN_PAGE_GOTO_NEXT);

        boolean bNext = false;    
        /*
        try {
            String strNext = request.getStringParameter(FN_PAGE_GOTO_NEXT);
            if(strNext != null && strNext.trim().length() != 0 && !strNext.equalsIgnoreCase("false")) {
                bNext = true;
            }
        } catch (ParameterNotFoundException pe) {
            // Ignore
        } catch (Exception ex) {
            //...
        }
        */
        return bNext;
    }
    
    public boolean doGotoNext()
    {
        if(isRequestGotoNext()) {
            return nextPage();
        } else {
            return false;
        }
    }

    // FN_PAGE_GOTO_PREVIOUS
    protected boolean isRequestGotoPrevious()
    {
        //System.out.println("Parsing " + FN_PAGE_GOTO_PREVIOUS);

        boolean bPrevious = false;    
        /*
        try {
            String strPrevious = request.getStringParameter(FN_PAGE_GOTO_PREVIOUS);
            if(strPrevious != null && strPrevious.trim().length() != 0 && !strPrevious.equalsIgnoreCase("false")) {
                bPrevious = true;
            }
        } catch (ParameterNotFoundException pe) {
            // Ignore
        } catch (Exception ex) {
            //...
        }
        */

        return bPrevious;
    }

    public boolean doGotoPrevious()
    {
        if(isRequestGotoPrevious()) {
            return previousPage();
        } else {
            return false;
        }
    }


    // FN_PAGE_GOTO_SPECIFIED
    protected int parseGotoSpecified()
    {
        //System.out.println("Parsing " + FN_PAGE_GOTO_SPECIFIED);

        int pg = -1;
        /*
        try {
            String strPage = request.getStringParameter(FN_PAGE_GOTO_SPECIFIED);
            if(strPage != null && strPage.trim().length() != 0) {
                //System.out.println("[parseGotoSpecified()] page = " + strPage);
                try {
                    pg = Integer.parseInt(strPage);
                } catch(NumberFormatException nfx) {
                    // ignore
                }
            }
        } catch (ParameterNotFoundException pe) {
            // Ignore
        } catch (Exception ex) {
            //...
        }
        */
        
        return pg;
    }

    public boolean doGotoSpecified()
    {
        int pg = parseGotoSpecified();
        if(pg != -1) {
            return gotoPage(pg);
        } else {
            return false;
        }
    }


    // Cloneable
    public Object clone() 
    {
        PaginationView bean = (PaginationView) super.clone();
            
        //...
        bean.mMaximumNumberOfItemsInPage = mMaximumNumberOfItemsInPage;
        bean.mItemCount = mItemCount;

        return bean;
    }
}

