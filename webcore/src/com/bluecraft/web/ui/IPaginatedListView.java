/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. This software is distributed on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 *************************************************************************/
// $Id: IPaginatedListView.java,v 1.2 2003/02/02 23:08:41 hyoon Exp $

package com.bluecraft.web.ui;

import  com.bluecraft.common.*;
import  com.bluecraft.web.*;


/**
 * The <code>IPaginatedListView</code> interface provides convenience routines
 * which manipulate items contained in a page.
 * The items need to be a type of IBean.
 *
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */
public interface IPaginatedListView
    extends IPaginationView
{
    /**
     * Returns the Item/IBean for the given index.
     *
     * @param index index.
     * @return an <code>IBean</code> value
     */
    IBean   getItem(int index);
}

