/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. This software is distributed on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 *************************************************************************/
// $Id: IPaginationView.java,v 1.2 2003/02/02 23:08:41 hyoon Exp $

package com.bluecraft.web.ui;

import  com.bluecraft.common.*;
import  com.bluecraft.web.*;


/**
 * The <code>IPaginationView</code> interface defines paging-related methods and fields.
 *
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.2 $
 */
public interface IPaginationView
{
    /**
     * Field name for "Goto Next page".
     * It's a boolean flag. 
     * (It'll be assumed to be true if the value is non-null, non-empty string,
     * as long as it is not equal to "false" or "0".)
     */
    static final String   FN_PAGE_GOTO_NEXT = "PaginationGotoNext";

    /**
     * Field name for "Goto Previous page".
     * It's a boolean flag. 
     * (It'll be assumed to be true if the value is non-null, non-empty string,
     * as long as it is not equal to "false" or "0".)
     */
    static final String   FN_PAGE_GOTO_PREVIOUS = "PaginationGotoPrevious";

    /**
     * Field name for "Goto the specified page".
     * The value is the page number.
     */
    static final String   FN_PAGE_GOTO_SPECIFIED = "PaginationGotoSpecified";

    
    /**
     * Returns the default/maximum number of items/objects per page.
     *
     * @return maximum number items in a page. 
     */
    int              getMaximumNumberOfItemsInPage();
    
    /**
     * Returns the total number of items/objects in the entire "list".
     *
     * @return item count.
     */
    int              getItemCount();
    
    /**
     * Returns the total number of pages.
     *
     * @return page count.
     */
    int              getPageCount();
    
    /**
     * Returns the page number of the "current" item.
     * Page number runs from 1 to getPageCount().
     * It returns 1 even if the list is empty, i.e. the current item is null.
     *
     * @return page number of the current item.
     */
    int              getPageNumber();
    
    /**
     * Returns the index of the first item in the page.
     * It returns 0 even if the list is empty, i.e. the current item is null.
     * (index runs from 0 to getItemCount()-1.)
     *
     * @return index of the first page.
     */
    int              getBeginIndex();
    
    /**
     * Returns the (index+1) of the last item in the page.
     * (index runs from 0 to getItemCount()-1.)
     *
     * @return last index.
     */
    int              getEndIndex();
    
    /**
     * Tests if the page number is greater than 1.
     *
     * @return true if the page number is greater than 1; false otherwise.
     */
    boolean          hasPrevious();
    
    /**
     * Returns true if the page number is smaller than getPageCount().
     *
     * @return true if the page number is smaller than getPageCount(); false otherwise.
     */
    boolean          hasNext();
    
    /**
     * Returns true if the given page number greater than or equal to 1 
     * and if it is smaller than or equals to getPageCount().
     *
     * @param page page number.
     * @return True if the page is valid; false otherwise.
     */
    boolean          isValidPage(int page);
    
    /**
     * Go to the previous page.
     * If the operation fails for any reason, it returns false.
     *
     * @return True if previous page is accessible; false otherwise.
     */
    boolean          previousPage();
    
    /**
     * Go to the next page.
     * If the operation fails for any reason, it returns false.
     *
     * @return True if next page is accessible; false otherwise.
     */
    boolean          nextPage();
    
    /**
     * Go to the given page.
     * If the operation fails for any reason, it returns false.
     *
     * @param page specified page number.
     * @return True if the page is accessible; false otherwise.
     */
    boolean          gotoPage(int page);

    /**
     * Go to the first page.
     * If the operation fails for any reason, it returns false.
     *
     * @return True if can goto the first page; false otherwise.
     */
    boolean          gotoFirstPage();

    /**
     * Go to the last page.
     * If the operation fails for any reason, it returns false.
     *
     * @return True if can goto the last page; false otherwise.
     */
    boolean          gotoLastPage();

}

