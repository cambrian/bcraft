/*************************************************************************
 *  Copyright (c) 2002-2003, BlueCraft Software. 
 *  All Rights Reserved. This software is distributed on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 *************************************************************************/
// $Id: BaseException.java,v 1.1.1.1 2003/12/25 06:55:18 hyoon Exp $

package com.bluecraft.ejbs;

import  com.bluecraft.common.*;


/**
 * BaseException is a super class of all exceptions thrown from the com.bluecraft.ejbs package.
 *   
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.1.1.1 $
 */   
public class BaseException
    extends Exception
{
    /**
     * Constructs a new BaseException.
     */
    public BaseException()
    {
        super();
    }
    
    /**
     * Constructs a new BaseException with the given message.
     *
     * @param message the detail message
     */
    public BaseException(String message)
    {
        super(message);
    }

    /**
     * Constructs a new BaseException with the specified detail message and cause. 
     *
     * @param message the detail message
     * @param cause the cause. null value means that the cause is nonexistent or unknown.
     */
    public BaseException(String message, Throwable cause)
    {
        super(message, cause);
    }

    /**
     * Constructs a new BaseException with the specified cause and a detail message of cause
     *
     * @param cause the cause. null value means that the cause is nonexistent or unknown.
     */
    public BaseException(Throwable cause)
    {
        super(cause);
    }

}

