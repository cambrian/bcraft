/*****************************************************************************
 * Copyright (c) 2002-2003, BlueCraft Software. 
 * This software is the proprietary information of Bluecraft Software
 * and it is supplied subject to license terms.
 * See its home page (http://webstone.sourceforge.net/) for more details.
 *****************************************************************************/
// $Id: DAOSystemException.java,v 1.1.1.1 2003/12/25 06:55:18 hyoon Exp $

package com.bluecraft.ejbs.util.dao;

import java.lang.RuntimeException;


/**
 * DAOSystemException is thrown by a DAO component when there is 
 * some irrecoverable error (like SQLException)
 */
public class DAOSystemException
    extends RuntimeException
{
    public DAOSystemException() {}
    public DAOSystemException(String msg) { super(msg); }
    public DAOSystemException(String msg, Throwable cause) {
        super(msg, cause);
    }
    public DAOSystemException(Throwable cause) {
        super(cause);
    }
}
