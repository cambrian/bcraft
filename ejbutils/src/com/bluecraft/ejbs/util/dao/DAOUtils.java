/*****************************************************************************
 * Copyright (c) 2002-2003, BlueCraft Software. 
 * This software is the proprietary information of Bluecraft Software
 * and it is supplied subject to license terms.
 * See its home page (http://webstone.sourceforge.net/) for more details.
 *****************************************************************************/
// $Id: DAOUtils.java,v 1.1.1.1 2003/12/25 06:55:18 hyoon Exp $

package com.bluecraft.ejbs.util.dao;

import com.bluecraft.ejbs.servicelocator.web.ServiceLocator;
import java.sql.*;
import javax.sql.*;
import javax.naming.*;


public class DAOUtils
{
    private DAOUtils() { }
    
    private static DataSource getDataSource(String dsName)
    throws DAOSystemException {
        String dataSourceName = ServiceLocator.getInstance().getString(dsName);
        return (DataSource) ServiceLocator.getInstance().getDataSource(dataSourceName);
    }
    
    public static Connection getDBConnection(String source)
    throws DAOSystemException {
        try {
            return getDataSource(source).getConnection();
        } catch (SQLException se) {
            throw new DAOSystemException("SQL Exception while getting "
            + "DB connection : \n" + se);
        }
    }
    
    public static void closeConnection(Connection dbConnection)
    throws DAOSystemException {
        try {
            if (dbConnection != null && !dbConnection.isClosed()) {
                dbConnection.close();
            }
        } catch (SQLException se) {
            throw new DAOSystemException("SQL Exception while closing "
            + "DB connection : \n" + se);
        }
    }
    
    public static void closeResultSet(ResultSet result)
    throws DAOSystemException {
        try {
            if (result != null) {
                result.close();
            }
        } catch (SQLException se) {
            throw new DAOSystemException("SQL Exception while closing "
            + "Result Set : \n" + se);
        }
    }
    
    public static void closeStatement(PreparedStatement stmt)
    throws DAOSystemException {
        try {
            if (stmt != null) {
                stmt.close();
            }
        } catch (SQLException se) {
            throw new DAOSystemException("SQL Exception while closing "
            + "Statement : \n" + se);
        }
    }
}
