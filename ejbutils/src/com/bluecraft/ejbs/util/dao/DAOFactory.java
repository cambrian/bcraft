/*****************************************************************************
 * Copyright (c) 2002-2003, BlueCraft Software. 
 * This software is the proprietary information of Bluecraft Software
 * and it is supplied subject to license terms.
 * See its home page (http://webstone.sourceforge.net/) for more details.
 *****************************************************************************/
// $Id: DAOFactory.java,v 1.1.1.1 2003/12/25 06:55:18 hyoon Exp $

package com.bluecraft.ejbs.util.dao;

import javax.naming.NamingException;
import javax.naming.InitialContext;

public class DAOFactory
{
    /**
     * This method instantiates the DAO class that is used in this
     * applications deployment environment to access the data. 
     * @param daoEnvEntry The env-entry in the deployment descriptor
     * that contains the name of the class corresponding to
     * which DAO to instantiate.
     *
     * @throws DAOSystemException if it has system error looking up 
     *         the DAO class name in the env-entry.
     */
    public static Object getDAO(String daoEnvEntry) throws DAOSystemException {
        try {
            InitialContext ic = new InitialContext();
            String className = (String) ic.lookup(daoEnvEntry);
            return Class.forName(className).newInstance();
        } catch (NamingException ne) {
            throw new DAOSystemException("DAOFactory.getDAO(" + daoEnvEntry +"):  NamingException while getting DAO type : \n" + ne.getMessage());
        } catch (Exception se) {
            throw new DAOSystemException("DAOFactory.getDAO(" + daoEnvEntry +"):  Exception while getting DAO type : \n" + se.getMessage());
        }
    }
}
