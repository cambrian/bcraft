/*****************************************************************************
 * Copyright (c) 2002-2003, BlueCraft Software. 
 * This software is the proprietary information of Bluecraft Software
 * and it is supplied subject to license terms.
 * See its home page (http://webstone.sourceforge.net/) for more details.
 *****************************************************************************/
// $Id: LogUtil.java,v 1.1.1.1 2003/12/25 06:55:18 hyoon Exp $

package com.bluecraft.ejbs;

import com.bluecraft.util.*;
import java.util.logging.Logger;


/**   
 * Utility class for Java logging framework.
 *
 * @author   <A href="mailto:hyoon@bluecraft.com">Hyoungsoo Yoon</A>
 * @version  $Revision: 1.1.1.1 $
 */
final class LogUtil
    extends LoggerUtil
{
    private static final String sName = "com.bluecraft.ejbs";

    private LogUtil()
    {
    }

    static Logger getLogger()
    {
        return Logger.getLogger(sName,getResourceBundle());
    }
  
}


