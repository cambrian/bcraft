@echo off

REM ----------------------------------------------------------
REM AuthServlet Main Build Script
REM This script is tested under Windows NT/2K & Win98
REM ----------------------------------------------------------

setlocal

REM -- Root directory for the project
set _PROJECTDIR=%PROJECTDIR%
set PROJECTDIR=E:/bcraft/tools


REM --------------------------------------------
REM No need to edit anything past here
REM --------------------------------------------

:init
set _CLASSPATH=%CLASSPATH%
set CLASSPATH=.;..;%CLASSPATH%
if exist %PROJECTDIR%/lib/bluecraft.common-0.9.jar set CLASSPATH=%CLASSPATH%;%PROJECTDIR%/lib/bluecraft.common-0.9.jar

:testjavahome
if "%JAVA_HOME%" == "" goto javahomeerror
goto build

:testanthome
if "%ANT_HOME%" == "" goto anthomeerror
goto build


:build
call ant %1
goto end


:javahomeerror
echo ERROR: JAVA_HOME not found in your environment.
echo Please, set the JAVA_HOME variable in your environment to match the
echo location of the Java Virtual Machine you want to use.

:anthomeerror
echo ERROR: ANT_HOME not found in your environment.
echo Please, set the ANT_HOME variable in your environment to match the
echo location of the ant installation.

:end
set CLASSPATH=%_CLASSPATH%
set PROJECTDIR=%_PROJECTDIR%

endlocal

