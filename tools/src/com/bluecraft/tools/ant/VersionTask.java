/*************************************************************************
 *  Copyright (c) 2001-2003, BlueCraft Software. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 *************************************************************************/
// $Id: VersionTask.java,v 1.1 2003/02/10 06:10:18 hyoon Exp $

package com.bluecraft.tools.ant;

import com.bluecraft.version.*;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;


/**
 * Ant custom task for updating build numbers.
 * It reads the current version from the version file specified as an argument,
 * and writes back a new version with the build number increased by one.
 * 
 * @author   Hyoungsoo Yoon
 * @version  $Revision: 1.1 $
 * @see      com.bluecraft.version.IVersion
 */
public class VersionTask
    extends com.bluecraft.ant.VersionTask
{
    // This is just a dummy class used to expose com.bluecraft.ant.VersionTask from the tools jar file.
}

