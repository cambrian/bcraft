/*************************************************************************
 *  Copyright (c) 2001-2003, BlueCraft Software. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 *************************************************************************/
// $Id: VersionStampTask.java,v 1.1 2003/02/11 22:32:26 hyoon Exp $

package com.bluecraft.tools.ant;

import com.bluecraft.version.*;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;


/**
 * Ant custom task for initializing version numbers so that they can be referenced in the build file.
 * It reads the current version from the version file specified as an argument,
 * and saves various version numbers as system environment variables.
 * 
 * @author   Hyoungsoo Yoon
 * @version  $Revision: 1.1 $
 * @see      com.bluecraft.version.IVersion
 */
public class VersionStampTask
    extends com.bluecraft.ant.VersionStampTask
{
    // This is just a dummy class used to expose com.bluecraft.ant.VersionStampTask from the tools jar file.
}
