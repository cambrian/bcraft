/*************************************************************************
 *  Copyright (c) 2001-2003, BlueCraft Software. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 *************************************************************************/
// $Id: GuidGenTask.java,v 1.1.1.1 2003/02/10 04:27:58 hyoon Exp $

package com.bluecraft.tools.ant;

import com.bluecraft.common.*;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;


/**
 * Ant custom task for generating Guids.
 * It prints out the generated Guids to STDOUT.
 * 
 * @author   Hyoungsoo Yoon
 * @version  $Revision: 1.1.1.1 $
 * @see      com.bluecraft.common.Guid
 */ 
public class GuidGenTask
    extends Task
{
    // The number of Guid to generated.
    // Its default value is 1.
    private int mReps = 1;

    /**
     * The method executing the task.
     * It generates new Guids and prints them out to STDOUT.
     * 
     * @throws org.apache.tools.ant.BuildException
     */
    public void execute()
        throws BuildException
    {
        for(int i=0;i<mReps;i++) {
            Guid guid = new Guid();
            System.out.println("New Guid = " + guid.toString());
        }
    }

    /**
     * The setter for the "reps" attribute.
     * The Guid generation will be repeated reps-times.
     * 
     * @param reps The number of Guids to be generated
     */
    public void setReps(int reps)
    {
        mReps = reps;
    }

}

