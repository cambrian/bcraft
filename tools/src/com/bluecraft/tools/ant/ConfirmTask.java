/*************************************************************************
 *  Copyright (c) 2001-2003, BlueCraft Software. All Rights Reserved.
 *  This software is distributed on an "AS IS" basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied.
 *************************************************************************/
// $Id: ConfirmTask.java,v 1.2 2003/02/24 02:15:52 hyoon Exp $

package com.bluecraft.tools.ant;

import com.bluecraft.common.*;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;


/**
 * Ant custom task for user "confirm".
 * It prints out preset question and waits for the user input.
 * 
 * @author   Hyoungsoo Yoon
 * @version  $Revision: 1.2 $
 */ 
public class ConfirmTask
    extends Task
{
    private static String YES = "y";
    private static String NO  = "n";

    // "Confirmation" question
    private String mQuestion = "";

    // Max wait in seconds.
    // Its default value is 10 secs.
    private int mTimeout = 10;

    // Default action
    private String mDefault = YES;


    /**
     * The method executing the task.
     * It prints out question and waits for the user input.
     * 
     * @throws org.apache.tools.ant.BuildException
     */
    public void execute()
        throws BuildException
    {
        System.out.println(mQuestion);
        // ....
        // TODO: If the ans == YES or the times out,
        // then execute the contained tasks here (??? Is it possible ???)
    }

    /**
     * The setter for the "question" attribute.
     * 
     * @param question the confirmation question
     */
    public void setQuestion(String question)
    {
        mQuestion = question;
    }

    /**
     * The setter for the "timeout" attribute.
     * The default action will be invoked if no user input is given within this time window.
     * 
     * @param timeout The time before the default action is invoked
     */
    public void setTimeout(int timeout)
    {
        mTimeout = timeout;
    }

    /**
     * The setter for the "default" attribute.
     * 
     * @param default the default action
     */
    public void setDefault(String def)
    {
        mDefault = def;
    }

}

