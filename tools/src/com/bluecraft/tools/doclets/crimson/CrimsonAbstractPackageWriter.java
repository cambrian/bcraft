/************************************************************************* 
 *  Copyright (C) 2002-2003. BlueCraft Software. All Rights Reserved. 
 *  This software is distributed on an "AS IS" basis, 
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. 
 *************************************************************************/ 
// $Id: CrimsonAbstractPackageWriter.java,v 1.1 2003/02/10 08:16:28 hyoon Exp $ 

package com.bluecraft.tools.doclets.crimson;

import com.sun.tools.doclets.*;
import com.sun.tools.doclets.standard.*;
import com.bluecraft.tools.doclets.*;
import com.sun.javadoc.*;
import java.io.*;
import java.lang.*;
import java.util.*;

/**   
 *   
 * @author   Hyoungsoo Yoon
 */   
public abstract class CrimsonAbstractPackageWriter extends AbstractPackageWriter { // extends HtmlStandardWriter {

    /**
     * Create appropriate directory for the package and also initilise the 
     * relative path from this generated file to the current or
     * the destination directory.
     *
     * @param path Directories in this path will be created if they are not 
     * already there.
     * @param filename Name of the package summary file to be generated.
     * @param packagedoc PackageDoc under consideration.
     * @throws DocletAbortException
     */
    public CrimsonAbstractPackageWriter(CrimsonConfigurationStandard configuration,
                                 String path, String filename, 
                                 PackageDoc packagedoc) 
                                 throws IOException {
        super(configuration, path, filename,packagedoc);
    }

}


