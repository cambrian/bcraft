/************************************************************************* 
 *  Copyright (C) 2002-2003. BlueCraft Software. All Rights Reserved. 
 *  This software is distributed on an "AS IS" basis, 
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. 
 *************************************************************************/ 
// $Id: CrimsonClassWriter.java,v 1.1 2003/02/10 08:16:28 hyoon Exp $ 

package com.bluecraft.tools.doclets.crimson;

import com.sun.tools.doclets.*;
import com.sun.tools.doclets.standard.*;
import com.bluecraft.tools.doclets.*;
import com.sun.javadoc.*;
import java.io.*;
import java.lang.*;
import java.util.*;


/**   
 *   
 * @author   Hyoungsoo Yoon
 */   
public class CrimsonClassWriter
    extends ClassWriter
{ // extends SubWriterHolderWriter {
    
    /**
     * @throws IOException
     * @throws DocletAbortException
     */
    public CrimsonClassWriter(CrimsonConfigurationStandard configuration,
                       String path, String filename, ClassDoc classdoc,
                       ClassDoc prev, ClassDoc next, ClassTree classtree,
                       boolean nopackage) throws IOException {
        super(configuration, path, filename, classdoc, prev, next, classtree, nopackage);
    }

}



