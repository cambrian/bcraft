/************************************************************************* 
 *  Copyright (C) 2002-2003. BlueCraft Software. All Rights Reserved. 
 *  This software is distributed on an "AS IS" basis, 
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. 
 *************************************************************************/ 
// $Id: CrimsonAbstractSubWriter.java,v 1.1 2003/02/10 08:16:28 hyoon Exp $ 

package com.bluecraft.tools.doclets.crimson;

import com.sun.tools.doclets.*;
import com.sun.tools.doclets.standard.*;
import com.bluecraft.tools.doclets.*;
import com.sun.tools.doclets.standard.tags.*;
import com.sun.javadoc.*;
import java.util.*;
import java.lang.reflect.Modifier;

/**   
 *   
 * @author   Hyoungsoo Yoon
 */   
public abstract class CrimsonAbstractSubWriter
    extends AbstractSubWriter
{ // extends AbstractSubWriter {

    public CrimsonAbstractSubWriter(CrimsonSubWriterHolderWriter writer,
                             ClassDoc classdoc) {
        super(writer, classdoc);
    }

    public CrimsonAbstractSubWriter(CrimsonSubWriterHolderWriter writer) {
        super(writer);
    }

}
    
