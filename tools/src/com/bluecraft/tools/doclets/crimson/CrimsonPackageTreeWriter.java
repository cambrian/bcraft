/************************************************************************* 
 *  Copyright (C) 2002-2003. BlueCraft Software. All Rights Reserved. 
 *  This software is distributed on an "AS IS" basis, 
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. 
 *************************************************************************/ 
// $Id: CrimsonPackageTreeWriter.java,v 1.1 2003/02/10 08:16:28 hyoon Exp $ 

package com.bluecraft.tools.doclets.crimson;

import com.sun.tools.doclets.*;
import com.sun.tools.doclets.standard.*;
import com.bluecraft.tools.doclets.*;
import com.sun.javadoc.*;
import java.io.*;
import java.lang.*;
import java.util.*;

/**   
 *   
 * @author   Hyoungsoo Yoon
 */   
public class CrimsonPackageTreeWriter extends PackageTreeWriter { // extends AbstractTreeWriter {


    /**
     * Constructor.
     * @throws IOException
     * @throws DocletAbortException
     */
    public CrimsonPackageTreeWriter(ConfigurationStandard configuration,
                             String path, String filename, 
                             PackageDoc packagedoc, 
                             PackageDoc prev, PackageDoc next,
                             boolean noDeprecated) 
                      throws IOException {
        super(configuration, path, filename, packagedoc, prev, next, noDeprecated);
    }

}
