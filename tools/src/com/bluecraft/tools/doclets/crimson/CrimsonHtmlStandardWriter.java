/************************************************************************* 
 *  Copyright (C) 2002-2003. BlueCraft Software. All Rights Reserved. 
 *  This software is distributed on an "AS IS" basis, 
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. 
 *************************************************************************/ 
// $Id: CrimsonHtmlStandardWriter.java,v 1.1 2003/02/10 08:16:28 hyoon Exp $ 

package com.bluecraft.tools.doclets.crimson;

import com.sun.tools.doclets.*;
import com.sun.tools.doclets.standard.*;
import com.bluecraft.tools.doclets.*;
import com.sun.tools.doclets.standard.tags.*;
import com.sun.javadoc.*;
import java.io.*;
import java.util.*;
import java.lang.*;
import java.text.MessageFormat;


/**   
 *   
 * @author   Hyoungsoo Yoon
 */   
public class CrimsonHtmlStandardWriter
    extends HtmlStandardWriter { // extends HtmlDocWriter {
    
    /**
     * Constructor to construct the HtmlStandardWriter object.
     *
     * @param filename File to be generated.
     */
    public CrimsonHtmlStandardWriter(CrimsonConfigurationStandard configuration,
                              String filename) throws IOException {
        super(configuration, filename);
    }
    
    /**
     * Constructor to construct the HtmlStandardWriter object.
     *
     * @param path         Platform-dependent {@link #path} used when 
     *                     creating file.
     * @param filename     Name of file to be generated.
     * @param relativepath Value for the variable {@link #relativepath}.
     */
    public CrimsonHtmlStandardWriter(CrimsonConfigurationStandard configuration,
                              String path, String filename,
                              String relativepath) throws IOException {
        super(configuration, path, filename, relativepath);
    }

}
