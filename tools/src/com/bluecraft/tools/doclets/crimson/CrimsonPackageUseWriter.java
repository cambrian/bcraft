/************************************************************************* 
 *  Copyright (C) 2002-2003. BlueCraft Software. All Rights Reserved. 
 *  This software is distributed on an "AS IS" basis, 
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. 
 *************************************************************************/ 
// $Id: CrimsonPackageUseWriter.java,v 1.1 2003/02/10 08:16:28 hyoon Exp $ 

package com.bluecraft.tools.doclets.crimson;

import com.sun.tools.doclets.*;
import com.sun.tools.doclets.standard.*;
import com.bluecraft.tools.doclets.*;
import com.sun.javadoc.*;
import java.io.*;
import java.util.*;

/**   
 *   
 * @author   Hyoungsoo Yoon
 */   
public class CrimsonPackageUseWriter
    extends PackageUseWriter
{ // extends SubWriterHolderWriter {
    
    /**
     * Constructor.
     *
     * @param filename the file to be generated.
     * @throws IOException
     * @throws DocletAbortException
     */
    public CrimsonPackageUseWriter(CrimsonConfigurationStandard configuration,
                            ClassUseMapper mapper, String filename,
                            PackageDoc pkgdoc) throws IOException {
        super(configuration, mapper, filename, pkgdoc);
    }

}
