/************************************************************************* 
 *  Copyright (C) 2002-2003. BlueCraft Software. All Rights Reserved. 
 *  This software is distributed on an "AS IS" basis, 
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. 
 *************************************************************************/ 
// $Id: CrimsonAllClassesFrameWriter.java,v 1.1 2003/02/10 08:16:28 hyoon Exp $ 

package com.bluecraft.tools.doclets.crimson;

import com.sun.javadoc.*;
import com.sun.tools.doclets.*;
import com.sun.tools.doclets.standard.*;
import com.bluecraft.tools.doclets.*;
import java.io.*;
import java.lang.*;
import java.util.*;


/**   
 *   
 * @author   Hyoungsoo Yoon
 */   
public class CrimsonAllClassesFrameWriter extends AllClassesFrameWriter { //extends HtmlStandardWriter {

    /**
     * Construct AllClassesFrameWriter object. Also initilises the indexbuilder
     * variable in this class.
     * @throws IOException
     * @throws DocletAbortException
     */
    public CrimsonAllClassesFrameWriter(CrimsonConfigurationStandard configuration,
                                 String filename, IndexBuilder indexbuilder)
                              throws IOException {
        super(configuration, filename, indexbuilder);
    }

}


