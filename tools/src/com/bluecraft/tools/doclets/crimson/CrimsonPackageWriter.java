/************************************************************************* 
 *  Copyright (C) 2002-2003. BlueCraft Software. All Rights Reserved. 
 *  This software is distributed on an "AS IS" basis, 
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. 
 *************************************************************************/ 
// $Id: CrimsonPackageWriter.java,v 1.1 2003/02/10 08:16:28 hyoon Exp $ 

package com.bluecraft.tools.doclets.crimson;

import com.sun.tools.doclets.*;
import com.sun.tools.doclets.standard.*;
import com.bluecraft.tools.doclets.*;
import com.sun.javadoc.*;
import java.io.*;
import java.lang.*;
import java.util.*;

/**   
 *   
 * @author   Hyoungsoo Yoon
 */   
public class CrimsonPackageWriter
    extends PackageWriter { // extends AbstractPackageWriter {

    /**
     * Constructor to construct PackageWriter object and to generate
     * "package-summary.html" file in the respective package directory.
     * For example for package "java.lang" this will generate file
     * "package-summary.html" file in the "java/lang" directory. It will also
     * create "java/lang" directory in the current or the destination directory
     * if it doesen't exist.
     *
     * @param path Directories in this path will be created if they are not
     * already there.
     * @param filename Name of the package summary file to be generated,
     * "package-frame.html".
     * @param packagedoc PackageDoc under consideration.
     * @param prev Previous package in the sorted array.
     * @param next Next package in the sorted array.
     * @throws IOException
     * @throws DocletAbortException
     */
    public CrimsonPackageWriter(ConfigurationStandard configuration,
                         String path, String filename,
                         PackageDoc packagedoc,
                         PackageDoc prev, PackageDoc next)
                         throws IOException {
        super(configuration, path, filename, packagedoc, prev, next);
    }

}




