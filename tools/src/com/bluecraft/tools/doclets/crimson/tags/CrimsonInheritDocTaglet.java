/************************************************************************* 
 *  Copyright (C) 2002-2003. BlueCraft Software. All Rights Reserved. 
 *  This software is distributed on an "AS IS" basis, 
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. 
 *************************************************************************/ 
// $Id: CrimsonInheritDocTaglet.java,v 1.1 2003/02/10 08:16:28 hyoon Exp $ 

package com.bluecraft.tools.doclets.crimson.tags;

import com.bluecraft.tools.doclets.crimson.*;
import com.sun.tools.doclets.*;
import com.sun.tools.doclets.standard.*;
import com.sun.tools.doclets.standard.tags.*;
import com.bluecraft.tools.doclets.*;
import com.sun.javadoc.*;


/**   
 *   
 * @author   Hyoungsoo Yoon
 */   
public class CrimsonInheritDocTaglet
    extends InheritDocTaglet
{ // extends AbstractInlineTaglet {
    

    /**
     * Construct a new InheritDocTaglet.
     */
    public CrimsonInheritDocTaglet () {
    } 

}
