/************************************************************************* 
 *  Copyright (C) 2002-2003. BlueCraft Software. All Rights Reserved. 
 *  This software is distributed on an "AS IS" basis, 
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. 
 *************************************************************************/ 
// $Id: CrimsonSimpleTaglet.java,v 1.1 2003/02/10 08:16:28 hyoon Exp $ 

package com.bluecraft.tools.doclets.crimson.tags;

import com.sun.javadoc.*;
import com.sun.tools.doclets.*;
import com.bluecraft.tools.doclets.crimson.*;
import com.bluecraft.tools.doclets.*;
import com.sun.tools.doclets.standard.*;
import com.sun.tools.doclets.standard.tags.*;
import java.util.*;


/**   
 *   
 * @author   Hyoungsoo Yoon
 */   
public class CrimsonSimpleTaglet
    extends SimpleTaglet
{ // implements Taglet {
    
    /**
     * Construct a <code>SimpleTaglet</code>.
     * @param tagName the name of this tag
     * @param header the header to output.
     * @param locations the possible locations that this tag
     * can appear in.  The <code>String</code> can contain 'p'
     * for package, 't' for type, 'm' for method, 'c' for constructor
     * and 'f' for field.
     */
    public CrimsonSimpleTaglet(String tagName, String header, String locations) {
        super(tagName, header, locations);
    }

}

