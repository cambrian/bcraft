/************************************************************************* 
 *  Copyright (C) 2002-2003. BlueCraft Software. All Rights Reserved. 
 *  This software is distributed on an "AS IS" basis, 
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. 
 *************************************************************************/ 
// $Id: CrimsonPackageFrameWriter.java,v 1.1 2003/02/10 08:16:28 hyoon Exp $ 

package com.bluecraft.tools.doclets.crimson;

import com.sun.tools.doclets.*;
import com.sun.tools.doclets.standard.*;
import com.bluecraft.tools.doclets.*;
import com.sun.javadoc.*;
import java.io.*;
import java.lang.*;
import java.util.*;
/**   
 *   
 * @author   Hyoungsoo Yoon
 */   
public class CrimsonPackageFrameWriter
    extends PackageFrameWriter { // extends AbstractPackageWriter {

    /**
     * Constructor to construct PackageFrameWriter object and to generate
     * "package-frame.html" file in the respective package directory.
     * For example for package "java.lang" this will generate file
     * "package-frame.html" file in the "java/lang" directory. It will also
     * create "java/lang" directory in the current or the destination directory
     * if it doesen't exist.
     *
     * @param path Directories in this path will be created if they are not
     * already there.
     * @param filename Name of the package summary file to be generated,
     * "package-frame.html".
     * @param packagedoc PackageDoc under consideration.
     * @throws IOException
     * @throws DocletAbortException
     */
    public CrimsonPackageFrameWriter(CrimsonConfigurationStandard configuration,
                              String path, String filename,
                              PackageDoc packagedoc)
                              throws IOException {
        super(configuration, path, filename, packagedoc);
    }

}



