/************************************************************************* 
 *  Copyright (C) 2002-2003. BlueCraft Software. All Rights Reserved. 
 *  This software is distributed on an "AS IS" basis, 
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. 
 *************************************************************************/ 
// $Id: CrimsonSerialMethodSubWriter.java,v 1.1 2003/02/10 08:16:28 hyoon Exp $ 

package com.bluecraft.tools.doclets.crimson;

import com.sun.javadoc.*;
import com.sun.tools.doclets.*;
import com.sun.tools.doclets.standard.*;
import com.bluecraft.tools.doclets.*;
import com.sun.tools.doclets.standard.tags.*;
import java.util.*;

/**   
 *   
 * @author   Hyoungsoo Yoon
 */   
public class CrimsonSerialMethodSubWriter
    extends SerialMethodSubWriter
{ // extends MethodSubWriter {

    public CrimsonSerialMethodSubWriter(CrimsonSubWriterHolderWriter writer,
                                 ClassDoc classdoc, CrimsonConfigurationStandard configuration) {
        super(writer, classdoc, configuration);
    }

}

