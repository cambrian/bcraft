/************************************************************************* 
 *  Copyright (C) 2002-2003. BlueCraft Software. All Rights Reserved. 
 *  This software is distributed on an "AS IS" basis, 
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. 
 *************************************************************************/ 
// $Id: CrimsonAbstractTreeWriter.java,v 1.1 2003/02/10 08:16:28 hyoon Exp $ 

package com.bluecraft.tools.doclets.crimson;

import com.sun.tools.doclets.*;
import com.sun.tools.doclets.standard.*;
import com.bluecraft.tools.doclets.*;
import com.sun.javadoc.*;
import java.io.*;
import java.lang.*;
import java.util.*;

/**   
 *   
 * @author   Hyoungsoo Yoon
 */   
public abstract class CrimsonAbstractTreeWriter extends AbstractTreeWriter { // extends HtmlStandardWriter {

    /**
     * Constructor initilises classtree variable. This constructor will be used
     * while generating global tree file "overview-tree.html".
     *
     * @param filename   File to be generated.
     * @param classtree  Tree built by {@link com.sun.tools.doclets.ClassTree}
     * @throws IOException
     * @throws DocletAbortException
     */
    protected CrimsonAbstractTreeWriter(CrimsonConfigurationStandard configuration,
                                 String filename, ClassTree classtree)
                                 throws IOException {
        super(configuration, filename, classtree);
    }
    
    /**
     * Create appropriate directory for the package and also initilise the
     * relative path from this generated file to the current or
     * the destination directory. This constructor will be used while
     * generating "package tree" file.
     *
     * @param path Directories in this path will be created if they are not
     * already there.
     * @param filename Name of the package tree file to be generated.
     * @param classtree The tree built using {@link com.sun.tools.doclets.ClassTree}
     * for the package pkg.
     * @param pkg PackageDoc for which tree file will be generated.
     * @throws IOException
     * @throws DocletAbortException
     */
    protected CrimsonAbstractTreeWriter(CrimsonConfigurationStandard configuration,
                                 String path, String filename,
                                 ClassTree classtree, PackageDoc pkg)
                                 throws IOException {
        super(configuration, path, filename, classtree, pkg);
    }

}
