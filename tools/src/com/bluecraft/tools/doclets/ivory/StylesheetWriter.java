/*
 * @(#)StylesheetWriter.java	1.14 01/12/03
 *
 * Copyright 2002 Sun Microsystems, Inc. All rights reserved.
 * SUN PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.bluecraft.tools.doclets.ivory;

import com.sun.tools.doclets.*;
import java.io.*;
import java.lang.*;
import java.util.*;

/**
 *
 * @author Atul M Dambalkar
 */
public class StylesheetWriter extends HtmlStandardWriter {

    /**
     * Constructor.
     */
    public StylesheetWriter(ConfigurationStandard configuration,
                            String filename) throws IOException {
        super(configuration, filename);
    }

    /**
     * Generate the style file contents.
     * @throws DocletAbortException 
     */
    public static void generate(ConfigurationStandard configuration) {
        StylesheetWriter stylegen;
        String filename = "";
        try {
            filename = "stylesheet.css";
            stylegen = new StylesheetWriter(configuration, filename);
            stylegen.generateStyleFile();
            stylegen.close();
        } catch (IOException exc) {
            configuration.standardmessage.error(
                        "doclet.exception_encountered",
                        exc.toString(), filename);
            throw new DocletAbortException();
        }
    }

    /**
     * Generate the style file contents.
     */
    protected void generateStyleFile() {
        print("/* "); printText("doclet.Style_line_1"); println(" */");
        println("");
        print("/* "); printText("doclet.Style_line_2"); println(" */");
        println("");
        print("/* "); printText("doclet.Style_line_3"); println(" */");
        println("body { background-color: #FFFFFF }"); println("");
        print("/* "); printText("doclet.Style_line_4"); println(" */");
        print(".TableHeadingColor     { background: #CCCCFF }"); 
        print(" /* "); printText("doclet.Style_line_5"); println(" */");
        print(".TableSubHeadingColor  { background: #EEEEFF }"); 
        print(" /* "); printText("doclet.Style_line_6"); println(" */");
        print(".TableRowColor         { background: #FFFFFF }"); 
        print(" /* "); printText("doclet.Style_line_7"); println(" */");
        println("");
        print("/* "); printText("doclet.Style_line_8"); println(" */");
        println(".FrameTitleFont   { font-size: 10pts; font-family: Helvetica, Arial, san-serif }");
        println(".FrameHeadingFont { font-size: 10pts; font-family: Helvetica, Arial, san-serif }");
        println(".FrameItemFont    { font-size: 10pts; font-family: Helvetica, Arial, san-serif }");
        println("");
        print("/* "); printText("doclet.Style_line_9"); println(" */");
        print("/* "); 
        print(".FrameItemFont  { font-size: 10pt; font-family: ");
        print("Helvetica, Arial, sans-serif }"); println(" */");
        println("");
        print("/* "); printText("doclet.Style_line_10"); println(" */");
        print(".NavBarCell1    { background-color:#EEEEFF;}");
        print("/* "); printText("doclet.Style_line_6"); println(" */");
        print(".NavBarCell1Rev { background-color:#00008B;}");
        print("/* "); printText("doclet.Style_line_11"); println(" */");

        print(".NavBarFont1    { font-family: Arial, Helvetica, sans-serif; ");
        println("color:#000000;}");
        print(".NavBarFont1Rev { font-family: Arial, Helvetica, sans-serif; ");
        println("color:#FFFFFF;}");
        println("");
        print(".NavBarCell2    { font-family: Arial, Helvetica, sans-serif; ");
        println("background-color:#FFFFFF;}");
        print(".NavBarCell3    { font-family: Arial, Helvetica, sans-serif; ");
        println("background-color:#FFFFFF;}");
        println("");

    }

}



