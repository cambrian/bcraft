/*
 * @(#)SerializedFormWriter.java	1.18 01/12/03
 *
 * Copyright 2002 Sun Microsystems, Inc. All rights reserved.
 * SUN PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.bluecraft.tools.doclets.ivory;

import com.sun.tools.doclets.*;
import com.sun.javadoc.*;
import java.io.*;
import java.lang.*;
import java.util.*;

/**
 * Generate the Serialized Form Information Page.
 *
 * @author Atul M Dambalkar
 */
public class SerializedFormWriter extends SubWriterHolderWriter {

    ConfigurationStandard configuration;
    
    /**
     * @throws IOException
     * @throws DocletAbortException
     */
    public SerializedFormWriter(ConfigurationStandard configuration,
                                String filename) throws IOException {
        super(configuration, filename);
        this.configuration = configuration;
    }
    
    /**
     * Generate a serialized form page.
     * @throws DocletAbortException
     */
    public static void generate(ConfigurationStandard configuration) {
        SerializedFormWriter serialgen;
        String filename = "serialized-form.html";
        try {
            serialgen = new SerializedFormWriter(configuration, filename);
            serialgen.generateSerializedFormFile(configuration.root);
            serialgen.close();
        } catch (IOException exc) {
            configuration.standardmessage.error(
                "doclet.exception_encountered",
                exc.toString(), filename);
            throw new DocletAbortException();
        }
    }
    
    /**
     * Generate the serialized form file.
     */
    public void generateSerializedFormFile(RootDoc root) {
        printHeader(getText("doclet.Serialized_Form"));
        navLinks(true);
        hr();
        
        center();
        h1(); printText("doclet.Serialized_Form"); h1End();
        centerEnd();
        
        generateContents(root);
        
        hr();
        navLinks(false);
        printBottom();
        printBodyHtmlEnd();
    }
    
    /**
     * Generate the serialized form file contents.
     */
    protected void generateContents(RootDoc root) {
        PackageDoc[] packages = root.specifiedPackages();
        ClassDoc[] cmdlineClasses = root.specifiedClasses();
        boolean first = true;
        for (int i = 0; i < packages.length; i++) {
            if (!serialInclude(packages[i])) {
                continue;
            }
            ClassDoc[] classes = packages[i].allClasses(false);
            boolean printPackageName = true;
            if (!serialClassFoundToDocument(classes)) {
                continue;
            }
            Arrays.sort(classes);
            for (int j = 0; j < classes.length; j++) {
                ClassDoc classdoc = classes[j];
                if(classdoc.isClass() && classdoc.isSerializable()) {
                    if(!serialClassInclude(classdoc)) {
                        continue;
                    }
                    if (printPackageName) {
                        hr(4, "noshade");
                        printPackageName(packages[i].name());
                        printPackageName = false;
                    }
                    first = false;
                    printSerialMemberInfo(classdoc);
                }
            }
        }
        if (cmdlineClasses.length > 0) {
            Arrays.sort(cmdlineClasses);
            for (int i = 0; i < cmdlineClasses.length; i++) {
                ClassDoc classdoc = cmdlineClasses[i];
                if(classdoc.isClass() && classdoc.isSerializable()) {
                    if (!first) {
                        hr(4, "noshade");
                    }
                    first = false;
                    printSerialMemberInfo(classdoc);
                }
            }
        }
    }
    
    /**
     * Return true if a serialized class is found with @serialinclude tag,
     * false otherwise.
     */
    protected boolean serialClassFoundToDocument(ClassDoc[] classes) {
        for (int i = 0; i < classes.length; i++) {
            if (serialClassInclude(classes[i])) {
                return true;
            }
        }
        return false;
    }
    
    
    /**
     * Print all the serializable member information.
     */
    protected void printSerialMemberInfo(ClassDoc cd) {
        //generate links backward only to public classes.
        String classlink = (cd.isPublic() || cd.isProtected())?
            getQualifiedClassLink(cd):
            cd.qualifiedName();
        anchor(cd.qualifiedName());
        printClassName(getText("doclet.Class_0_implements_serializable",
                               classlink));
        
        printSerialMembers(cd);
        p();
    }
    
    /**
     * Print summary and detail information for the serial members in the
     * class.
     */
    protected void printSerialMembers(ClassDoc cd) {
        new SerialMethodSubWriter(this, cd, configuration).printMembers();
        new SerialFieldSubWriter(this, cd).printMembers();
    }
    
    /**
     * Print the package name in the table format.
     */
    protected void printPackageName(String pkgname) {
        tableHeader();
        tdAlign("center");
        font("+2");
        boldText("doclet.Package");
        print(' ');
        bold(pkgname);
        tableFooter();
    }
    
    protected void printClassName(String classstr) {
        tableHeader();
        tdColspan(2);
        font("+2");
        bold(classstr);
        tableFooter();
    }
    
    protected void tableHeader() {
        tableIndexSummary();
        trBgcolorStyle("#CCCCFF", "TableSubHeadingColor");
    }
    
    protected void tableFooter() {
        fontEnd();
        tdEnd(); trEnd(); tableEnd();
        p();
    }
}


