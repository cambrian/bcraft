/*
 * @(#)AbstractExecutableMemberTaglet.java	1.5 01/12/03
 *
 * Copyright 2002 Sun Microsystems, Inc. All rights reserved.
 * SUN PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.bluecraft.tools.doclets.ivory.tags;

import com.bluecraft.tools.doclets.ivory.*;
import com.sun.tools.doclets.*;
import com.sun.javadoc.*;

/**
 * An abstract class for Taglets in
 * <code>ExecutableMembers</code> in the standard doclet.
 * @author Jamie Ho
 * @since 1.4
 */
public abstract class AbstractExecutableMemberTaglet implements Taglet{
    
    protected String name = "Default";
    
    /**
     * Return true if this <code>Taglet</code>
     * is used in constructor documentation.
     * @return true if this <code>Taglet</code>
     * is used in constructor documentation and false
     * otherwise.
     */
    public boolean inConstructor() {
        return true;
    }
    
    /**
     * Return true if this <code>Taglet</code>
     * is used in field documentation.
     * @return true if this <code>Taglet</code>
     * is used in field documentation and false
     * otherwise.
     */
    public boolean inField() {
        return false;
    }
    
    /**
     * Return true if this <code>Taglet</code>
     * is used in method documentation.
     * @return true if this <code>Taglet</code>
     * is used in method documentation and false
     * otherwise.
     */
    public boolean inMethod() {
        return true;
    }
    
    /**
     * Return true if this <code>Taglet</code>
     * is used in overview documentation.
     * @return true if this <code>Taglet</code>
     * is used in method documentation and false
     * otherwise.
     */
    public boolean inOverview() {
        return false;
    }
    
    /**
     * Return true if this <code>Taglet</code>
     * is used in package documentation.
     * @return true if this <code>Taglet</code>
     * is used in package documentation and false
     * otherwise.
     */
    public boolean inPackage() {
        return false;
    }
    
    /**
     * Return true if this <code>Taglet</code>
     * is used in type documentation (classes or interfaces).
     * @return true if this <code>Taglet</code>
     * is used in type documentation and false
     * otherwise.
     */
    public boolean inType() {
        return false;
    }
    
    /**
     * Return true if this <code>Taglet</code>
     * is an inline tag.
     * @return true if this <code>Taglet</code>
     * is an inline tag and false otherwise.
     */
    public boolean isInlineTag() {
        return false;
    }
    
    /**
     * Return the name of this custom tag.
     * @return the name of this custom tag.
     */
    public String getName() {
        return name;
    }
    
    /**
     * This method always returns "" because this <code>Taglet</code> requires more
     * information to convert a Tag to a String.
     * @param tag the <code>Tag</code> representation of this custom tag.
     * @return an empty String.
     */
    public String toString(Tag tag) {
        return "";
    }
    
    /**
     * This method always returns "" because this <code>Taglet</code>  requires more
     * information to convert a Tag to a String.
     * @param tags an array of <code>Tag</code>s representing of this custom tag.
     * @return an empty String.
     */
    public String toString(Tag[] tags){
        return "";
    }
    
    /**
     * Given a method, find a method that it overrides or implements
     * so that we can inherit documentation.
     * @param method the method to inherit documentation from.
     */
    
    protected MethodDoc getInheritedMethodDoc(MethodDoc method){
        //Find method to inherit tags from.
        TaggedMethodFinder finder = new TaggedMethodFinder();
        MethodDoc inheritedMethod = finder.search(method.containingClass(), method);
        if (inheritedMethod == null) {
            //check interfaces since method is not in superclass.
            ClassDoc[] interfaces = method.containingClass().interfaces();
            for (int i = 0; i < interfaces.length; i++) {
                inheritedMethod = finder.search(interfaces[i], method);
                if (inheritedMethod != null) {
                    return inheritedMethod;
                }
            }
        }
        return inheritedMethod;
    }
    
    public abstract String toString(Doc holder, HtmlStandardWriter writer);
}
