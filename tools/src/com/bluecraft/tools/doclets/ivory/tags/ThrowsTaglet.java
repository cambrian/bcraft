/*
 * @(#)ThrowsTaglet.java	1.9 02/04/14
 *
 * Copyright 2002 Sun Microsystems, Inc. All rights reserved.
 * SUN PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.bluecraft.tools.doclets.ivory.tags;

import com.bluecraft.tools.doclets.ivory.*;
import com.sun.tools.doclets.*;
import com.sun.javadoc.*;
import java.util.*;

/**
 * A taglet that represents the @throws tag.
 * @author Jamie Ho
 * @since 1.4
 */
public class ThrowsTaglet extends AbstractExecutableMemberTaglet{
    
    public ThrowsTaglet() {
        name = "throws";
    }
      
    /**
     * Given an array of <code>Tag</code>s representing this custom
     * tag, return its string representation. If possible, inherit the throws tags
     * for undocumented declared exceptions.
     * @param tags the array of <code>ThrowsTag</code>s representing of this custom tag.
     * @param holder the ExecutableMemberDoc that holds this tag.
     * @param writer the HtmlStandardWriter that will write this tag.
     * @return the string representation of this <code>Tag</code>.
     */
    public String toString(Doc holder, HtmlStandardWriter writer) {
        ExecutableMemberDoc execHolder = (ExecutableMemberDoc) holder;
        ThrowsTag[] tags = execHolder.throwsTags();
        String result = "";
        MethodDoc inheritedMethod;
        HashSet alreadyDocumented = new HashSet();
        if (tags.length > 0) {
            result += throwsTagsToString(execHolder,
                                         execHolder.throwsTags(),
                                         writer,
                                         alreadyDocumented,
                                         true);
        }
        ClassDoc[] declaredExceptionCDs = execHolder.thrownExceptions();
        if (holder instanceof MethodDoc &&
                (inheritedMethod = getInheritedMethodDoc((MethodDoc) holder)) != null) {
            //Inherit @throws tags only for declared exceptions.
            HashSet declaredExceptions = new HashSet();
            
            //Save all declared exceptions
            for (int j = 0; j < declaredExceptionCDs.length; j++) {
                declaredExceptions.add(declaredExceptionCDs[j]);
            }
            //Inherit @throws tags for declared exceptions
            HashSet declaredExceptionTags = new HashSet();
            ThrowsTag[] throwsTags = inheritedMethod.throwsTags();
            for (int j = 0; j < throwsTags.length; j++) {
                if (declaredExceptions.contains(throwsTags[j].exception())) {
                    declaredExceptionTags.add(throwsTags[j]);
                }
            }
            result += throwsTagsToString(execHolder,
                                         (ThrowsTag[]) declaredExceptionTags.toArray(new ThrowsTag[] {}),
                                         writer,
                                         alreadyDocumented,
                                         false);
        }
        
        //Add links to the exceptions declared but not documented.
        for (int i = 0; i < declaredExceptionCDs.length; i++) {
            if (declaredExceptionCDs[i] != null &&
                ! alreadyDocumented.contains(declaredExceptionCDs[i].name()) &&
                ! alreadyDocumented.contains(declaredExceptionCDs[i].qualifiedName())) {
                result += "\n<DD>";
                result += writer.codeText(writer.getClassLink(declaredExceptionCDs[i]));
                alreadyDocumented.add(declaredExceptionCDs[i].name());
            }
        }
        
        return result.equals("") ? null : result;
    }
    
    /**
     * Given an array of <code>Tag</code>s representing this custom
     * tag, return its string representation.
     * @param holder the ExecutableMemberDoc that holds this tag.
     * @param throwsTags the array of <code>ThrowsTag</code>s to convert.
     * @param writer the HtmlStandardWriter that will write this tag.
     * @param alreadyDocumented the set of exceptions that have already
     *        been documented.
     * @param allowDups True if we allow duplicate throws tags to be documented.
     * @return the string representation of this <code>Tag</code>.
     */
    protected String throwsTagsToString(ExecutableMemberDoc holder,
                                        ThrowsTag[] throwTags,
                                        HtmlStandardWriter writer,
                                        Set alreadyDocumented,
                                        boolean allowDups) {
        String result = "";
        if (throwTags.length > 0) {
            for (int i = 0; i < throwTags.length; ++i) {
                ThrowsTag tt = throwTags[i];
                ClassDoc cd = tt.exception();
                if ((!allowDups) && (alreadyDocumented.contains(tt.exceptionName()) ||
                          (cd != null && alreadyDocumented.contains(cd.qualifiedName())))) {
                    continue;
                }
                if (alreadyDocumented.size() == 0) {
                    result += "\n<DT>" + "<B>" + writer.getText("doclet.Throws") + "</B>";
                }
                result += "\n<DD>";
                if (cd == null) {
                    result += writer.codeText(tt.exceptionName());
                } else {
                    result += writer.codeText(writer.getClassLink(cd));
                }
                String text = writer.commentTagsToString(null, tt.inlineTags(), false, false);
                if (holder instanceof MethodDoc) {
                    text = writer.replaceInheritDoc((MethodDoc) holder, tt, text);
                }
                if (text != null && text.length() > 0) {
                    result += " - ";
                }
                result += text;
                alreadyDocumented.add(cd != null ? cd.qualifiedName() : tt.exceptionName());
            }
        }
        return result;
    }
}
