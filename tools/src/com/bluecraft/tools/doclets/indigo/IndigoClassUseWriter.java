/************************************************************************* 
 *  Copyright (C) 2002-2003. BlueCraft Software. All Rights Reserved. 
 *  This software is distributed on an "AS IS" basis, 
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. 
 *************************************************************************/ 
// $Id: IndigoClassUseWriter.java,v 1.1.1.1 2003/02/10 04:27:58 hyoon Exp $ 

package com.bluecraft.tools.doclets.indigo;

import com.sun.tools.doclets.*;
import com.bluecraft.tools.doclets.ivory.*;
import com.bluecraft.tools.doclets.*;
import com.sun.javadoc.*;
import java.io.*;
import java.lang.*;
import java.util.*;

/**   
 *   
 * @author   Hyoungsoo Yoon
 */   
public class IndigoClassUseWriter
    extends ClassUseWriter
{ // extends SubWriterHolderWriter {
    
    /**
     * Constructor.
     *
     * @param filename the file to be generated.
     * @throws IOException
     * @throws DocletAbortException
     */
    public IndigoClassUseWriter(IndigoConfigurationStandard configuration,
                          ClassUseMapper mapper, String path,
                          String filename, String relpath,
                          ClassDoc classdoc) throws IOException {
        super(configuration, mapper, path, filename, relpath, classdoc);
    }

}

