/************************************************************************* 
 *  Copyright (C) 2002-2003. BlueCraft Software. All Rights Reserved. 
 *  This software is distributed on an "AS IS" basis, 
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. 
 *************************************************************************/ 
// $Id: IndigoSerialMethodSubWriter.java,v 1.1.1.1 2003/02/10 04:27:58 hyoon Exp $ 

package com.bluecraft.tools.doclets.indigo;

import com.sun.javadoc.*;
import com.sun.tools.doclets.*;
import com.bluecraft.tools.doclets.ivory.*;
import com.bluecraft.tools.doclets.*;
import com.bluecraft.tools.doclets.ivory.tags.*;
import java.util.*;

/**   
 *   
 * @author   Hyoungsoo Yoon
 */   
public class IndigoSerialMethodSubWriter
    extends SerialMethodSubWriter
{ // extends MethodSubWriter {

    public IndigoSerialMethodSubWriter(IndigoSubWriterHolderWriter writer,
                                 ClassDoc classdoc, IndigoConfigurationStandard configuration) {
        super(writer, classdoc, configuration);
    }

}

