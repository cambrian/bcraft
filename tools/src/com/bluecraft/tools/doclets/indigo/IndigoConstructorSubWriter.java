/************************************************************************* 
 *  Copyright (C) 2002-2003. BlueCraft Software. All Rights Reserved. 
 *  This software is distributed on an "AS IS" basis, 
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. 
 *************************************************************************/ 
// $Id: IndigoConstructorSubWriter.java,v 1.1.1.1 2003/02/10 04:27:58 hyoon Exp $ 

package com.bluecraft.tools.doclets.indigo;

import com.sun.tools.doclets.*;
import com.bluecraft.tools.doclets.ivory.*;
import com.bluecraft.tools.doclets.*;
import com.bluecraft.tools.doclets.ivory.tags.*;
import com.sun.javadoc.*;
import java.util.*;

/**   
 *   
 * @author   Hyoungsoo Yoon
 */   
public class IndigoConstructorSubWriter extends ConstructorSubWriter { // extends ExecutableMemberSubWriter {
    
    public IndigoConstructorSubWriter(IndigoSubWriterHolderWriter writer,
                                ClassDoc classdoc) {
        super(writer, classdoc);
    }
    
    public IndigoConstructorSubWriter(SubWriterHolderWriter writer) {
        super(writer);
    }

}


