/************************************************************************* 
 *  Copyright (C) 2002-2003. BlueCraft Software. All Rights Reserved. 
 *  This software is distributed on an "AS IS" basis, 
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. 
 *************************************************************************/ 
// $Id: IndigoPackageIndexWriter.java,v 1.1.1.1 2003/02/10 04:27:58 hyoon Exp $ 

package com.bluecraft.tools.doclets.indigo;

import com.sun.tools.doclets.*;
import com.bluecraft.tools.doclets.ivory.*;
import com.bluecraft.tools.doclets.*;
import com.sun.javadoc.*;
import java.io.*;
import java.lang.*;
import java.util.*;

/**   
 *   
 * @author   Hyoungsoo Yoon
 */   
public class IndigoPackageIndexWriter
    extends PackageIndexWriter { // extends AbstractPackageIndexWriter {

    /**
     * Construct the PackageIndexWriter. Also constructs the grouping 
     * information as provided on the command line by "-group" option. Stores 
     * the order of groups specified by the user.
     *
     * @see Group
     */
    public IndigoPackageIndexWriter(IndigoConfigurationStandard configuration,
                              String filename) 
                       throws IOException {
        super(configuration, filename);
    }

}



