/************************************************************************* 
 *  Copyright (C) 2002-2003. BlueCraft Software. All Rights Reserved. 
 *  This software is distributed on an "AS IS" basis, 
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. 
 *************************************************************************/ 
// $Id: IndigoTreeWriter.java,v 1.1.1.1 2003/02/10 04:27:58 hyoon Exp $ 

package com.bluecraft.tools.doclets.indigo;

import com.sun.tools.doclets.*;
import com.bluecraft.tools.doclets.ivory.*;
import com.bluecraft.tools.doclets.*;
import com.sun.javadoc.*;
import java.io.*;
import java.lang.*;
import java.util.*;

/**   
 *   
 * @author   Hyoungsoo Yoon
 */   
public class IndigoTreeWriter extends TreeWriter { // extends AbstractTreeWriter {


    /**
     * Constructor to construct TreeWriter object.
     *
     * @param file String filename
     * @param classtree Tree built using
     * {@link com.sun.tools.doclets.ClassTree}.
     * @throws DocletAbortException
     */
    public IndigoTreeWriter(IndigoConfigurationStandard configuration,
                      String filename, ClassTree classtree)  
                                 throws IOException {
        super(configuration, filename, classtree);
    }

}
