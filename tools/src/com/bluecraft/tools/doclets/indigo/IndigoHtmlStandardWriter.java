/************************************************************************* 
 *  Copyright (C) 2002-2003. BlueCraft Software. All Rights Reserved. 
 *  This software is distributed on an "AS IS" basis, 
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. 
 *************************************************************************/ 
// $Id: IndigoHtmlStandardWriter.java,v 1.1.1.1 2003/02/10 04:27:58 hyoon Exp $ 

package com.bluecraft.tools.doclets.indigo;

import com.sun.tools.doclets.*;
import com.bluecraft.tools.doclets.ivory.*;
import com.bluecraft.tools.doclets.*;
import com.bluecraft.tools.doclets.ivory.tags.*;
import com.sun.javadoc.*;
import java.io.*;
import java.util.*;
import java.lang.*;
import java.text.MessageFormat;


/**   
 *   
 * @author   Hyoungsoo Yoon
 */   
public class IndigoHtmlStandardWriter
    extends HtmlStandardWriter { // extends HtmlDocWriter {
    
    /**
     * Constructor to construct the HtmlStandardWriter object.
     *
     * @param filename File to be generated.
     */
    public IndigoHtmlStandardWriter(IndigoConfigurationStandard configuration,
                              String filename) throws IOException {
        super(configuration, filename);
    }
    
    /**
     * Constructor to construct the HtmlStandardWriter object.
     *
     * @param path         Platform-dependent {@link #path} used when 
     *                     creating file.
     * @param filename     Name of file to be generated.
     * @param relativepath Value for the variable {@link #relativepath}.
     */
    public IndigoHtmlStandardWriter(IndigoConfigurationStandard configuration,
                              String path, String filename,
                              String relativepath) throws IOException {
        super(configuration, path, filename, relativepath);
    }

}
