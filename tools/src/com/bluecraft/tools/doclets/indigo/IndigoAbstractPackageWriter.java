/************************************************************************* 
 *  Copyright (C) 2002-2003. BlueCraft Software. All Rights Reserved. 
 *  This software is distributed on an "AS IS" basis, 
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. 
 *************************************************************************/ 
// $Id: IndigoAbstractPackageWriter.java,v 1.1.1.1 2003/02/10 04:27:58 hyoon Exp $ 

package com.bluecraft.tools.doclets.indigo;

import com.sun.tools.doclets.*;
import com.bluecraft.tools.doclets.ivory.*;
import com.bluecraft.tools.doclets.*;
import com.sun.javadoc.*;
import java.io.*;
import java.lang.*;
import java.util.*;

/**   
 *   
 * @author   Hyoungsoo Yoon
 */   
public abstract class IndigoAbstractPackageWriter extends AbstractPackageWriter { // extends HtmlStandardWriter {

    /**
     * Create appropriate directory for the package and also initilise the 
     * relative path from this generated file to the current or
     * the destination directory.
     *
     * @param path Directories in this path will be created if they are not 
     * already there.
     * @param filename Name of the package summary file to be generated.
     * @param packagedoc PackageDoc under consideration.
     * @throws DocletAbortException
     */
    public IndigoAbstractPackageWriter(IndigoConfigurationStandard configuration,
                                 String path, String filename, 
                                 PackageDoc packagedoc) 
                                 throws IOException {
        super(configuration, path, filename,packagedoc);
    }

}



