/************************************************************************* 
 *  Copyright (C) 2002-2003. BlueCraft Software. All Rights Reserved. 
 *  This software is distributed on an "AS IS" basis, 
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. 
 *************************************************************************/ 
// $Id: IndigoSimpleTaglet.java,v 1.1.1.1 2003/02/10 04:27:58 hyoon Exp $ 

package com.bluecraft.tools.doclets.indigo.tags;

import com.sun.javadoc.*;
import com.sun.tools.doclets.*;
import com.bluecraft.tools.doclets.indigo.*;
import com.bluecraft.tools.doclets.*;
import com.bluecraft.tools.doclets.ivory.*;
import com.bluecraft.tools.doclets.ivory.tags.*;
import java.util.*;


/**   
 *   
 * @author   Hyoungsoo Yoon
 */   
public class IndigoSimpleTaglet
    extends SimpleTaglet
{ // implements Taglet {
    
    /**
     * Construct a <code>SimpleTaglet</code>.
     * @param tagName the name of this tag
     * @param header the header to output.
     * @param locations the possible locations that this tag
     * can appear in.  The <code>String</code> can contain 'p'
     * for package, 't' for type, 'm' for method, 'c' for constructor
     * and 'f' for field.
     */
    public IndigoSimpleTaglet(String tagName, String header, String locations) {
        super(tagName, header, locations);
    }

}

