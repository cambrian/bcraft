/************************************************************************* 
 *  Copyright (C) 2002-2003. BlueCraft Software. All Rights Reserved. 
 *  This software is distributed on an "AS IS" basis, 
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. 
 *************************************************************************/ 
// $Id: IndigoDocRootTaglet.java,v 1.1.1.1 2003/02/10 04:27:58 hyoon Exp $ 

package com.bluecraft.tools.doclets.indigo.tags;

import com.bluecraft.tools.doclets.indigo.*;
import com.sun.tools.doclets.*;
import com.bluecraft.tools.doclets.ivory.*;
import com.bluecraft.tools.doclets.ivory.tags.*;
import com.bluecraft.tools.doclets.*;
import com.sun.javadoc.*;


/**   
 *   
 * @author   Hyoungsoo Yoon
 */   
public class IndigoDocRootTaglet
    extends DocRootTaglet
{ // extends AbstractInlineTaglet {

    /**
     * Construct a new DocRootTaglet.
     */
    public IndigoDocRootTaglet() {
    } 

}
