/************************************************************************* 
 *  Copyright (C) 2002-2003. BlueCraft Software. All Rights Reserved. 
 *  This software is distributed on an "AS IS" basis, 
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. 
 *************************************************************************/ 
// $Id: IndigoSplitIndexWriter.java,v 1.1.1.1 2003/02/10 04:27:58 hyoon Exp $ 

package com.bluecraft.tools.doclets.indigo;

import com.sun.tools.doclets.*;
import com.bluecraft.tools.doclets.ivory.*;
import com.bluecraft.tools.doclets.*;
import com.sun.javadoc.*;
import java.io.*;
import java.lang.*;
import java.util.*;

/**   
 *   
 * @author   Hyoungsoo Yoon
 */   
public class IndigoSplitIndexWriter
    extends SplitIndexWriter
{ // extends AbstractIndexWriter {

    /**
     * Construct the SplitIndexWriter. Uses path to this file and relative path
     * from this file.
     * 
     * @param path       Path to the file which is getting generated.
     * @param filename   Name of the file which is getting genrated.
     * @param relpath    Relative path from this file to the current directory.
     * @param indexbuilder Unicode based Index from {@link IndexBuilder}
     */
    public IndigoSplitIndexWriter(IndigoConfigurationStandard configuration,
                            String path, String filename, 
                            String relpath, IndexBuilder indexbuilder,
                            int prev, int next) throws IOException {
        super(configuration, path, filename, relpath, indexbuilder, prev, next);
    }

}
