/************************************************************************* 
 *  Copyright (C) 2002-2003. BlueCraft Software. All Rights Reserved. 
 *  This software is distributed on an "AS IS" basis, 
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. 
 *************************************************************************/ 
// $Id: IndigoPackageTreeWriter.java,v 1.1.1.1 2003/02/10 04:27:58 hyoon Exp $ 

package com.bluecraft.tools.doclets.indigo;

import com.sun.tools.doclets.*;
import com.bluecraft.tools.doclets.ivory.*;
import com.bluecraft.tools.doclets.*;
import com.sun.javadoc.*;
import java.io.*;
import java.lang.*;
import java.util.*;

/**   
 *   
 * @author   Hyoungsoo Yoon
 */   
public class IndigoPackageTreeWriter extends PackageTreeWriter { // extends AbstractTreeWriter {


    /**
     * Constructor.
     * @throws IOException
     * @throws DocletAbortException
     */
    public IndigoPackageTreeWriter(ConfigurationStandard configuration,
                             String path, String filename, 
                             PackageDoc packagedoc, 
                             PackageDoc prev, PackageDoc next,
                             boolean noDeprecated) 
                      throws IOException {
        super(configuration, path, filename, packagedoc, prev, next, noDeprecated);
    }

}
